# README #


## Homework assignment 3 : ##

   Original source repository on [Github](http://code.google.com/p/json-simple/)

### Steps to execute the launcher application: ###

* Through cmd/terminal, go to project root directory.
* Run commands in sequence:
	* `gradle clean`  -> To clear the compiled gradle classes.
	* `gradle build` -> To re-build our compiled class files.
	* `gradle run` -> To run our main class from gradle.
* The matching traces and the mismatch are printed on the console.

### Code Orgaization ###
* Main Launcher : `org.smaith2.jsonParser.mutator.Main`
* Location of mutators : `org.smaith2.jsonParser.mutator.Ast.Modifiers.*`
* Matrix mapping expression types to potential mutataions : `org.smaith2.jsonParser.mutator.helper.MutationMatrix` 
* Identifying location for mutation : `org.smaith2.jsonParser.mutator.Ast.BlockSelector`

### Functionalities implemented: ###
* #### Exact location for mutation is identified based on: ####
	* Class name
	* Function name
	* Complete function desciptor (includes method parameters, return types and exceptions thrown)
	* Path to expression including nested constructs such as While, If, Switch
	* Exact match of actual expression 
	
* #### Implemented Mutations ####  
	* AORB : Arithmetic operator replacement for binary operators 
	* AORB2 : Arithmetic operator replacement for binary operators (2nd type)
	* COD : Conditional operator deletion
	* COR : Conditional operator Replacement
	* IPC : Explicit call to parent's constructor deletion
	* JID : Member variable initialization deletion
	* JTD : `this` keyword deletion
	* PCD : Type cast operator deletion
	* ROR : Relational operator deletion
	
* #### Random regex generation using the Generex library: ####
	* created config.properties file in resources folder.
	* The config file contains key values,we are storing different propeties like input_category,input_type, init_function,execute_function.Input category supports json,array(string,integer),string,integer.
	* Based on category the input value of specific type is generated randomly. 
	* For this project random json is generated 5 times of different length and unique structure,passed as an input parameter.
	
* #### Optmizing execution trace logging: ####
	* Initially traces are stored in memory. Unique instrumentation logs are stored in a HashMap with the MD5 of the object acting as a key, and logs for individual runs contain this key in order of occurence
	* In a typical run, the hash contains about 300 unique entries, while the full logs contain about 3000 to 5000 entries which are fixed length representations of log objects
	* The traces are then modified to form a hashTable based on key value pairs. Where key is the combination of the ClassName and MethodName. Value is basically an arraylist of mutations applied.
	* Then Java executor service is used to launch threads for each method and class to apply mutatations.
	* The mutated result is collected in a hash map again of Key value pairs for comparison.	