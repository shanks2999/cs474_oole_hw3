package org.smaith2.jsonParser.mutator;

import com.github.javaparser.ast.visitor.ModifierVisitor;
import javafx.util.Pair;
import javassist.CannotCompileException;
import javassist.CtClass;
import org.smaith2.jsonParser.console.ParserApplication;
import org.smaith2.jsonParser.console.instrumentation.LogMap;
import org.smaith2.jsonParser.console.instrumentation.LogRecord;
import org.smaith2.jsonParser.console.instrumentation.LogResult;
import org.smaith2.jsonParser.mutator.Ast.SourceModifier2;
import org.smaith2.jsonParser.mutator.configRegex.ReverseRegex;
import org.smaith2.jsonParser.mutator.helper.MutationMatrix;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.*;

public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
//        ReverseRegex.readConfigFile().getJson();
        /*String[] parameters = new String[]{
                "{}",
                "{\"2\":{\"3\":{\"4\":[5,{\"6\":7}]}}}",
                "[5,]",
                "[5,,2]",
                "\"[\\\"hello\\\\bworld\\\\\\\"abc\\\\tdef\\\\\\\\ghi\\\\rjkl\\\\n123\\\\u4e2d\\\"]\"",
                "[\"hello\\bworld\\\"abc\\tdef\\\\ghi\\rjkl\\n123\\u4e2d\"]",
                "[0,{\"1\":{\"2\":{\"3\":{\"4\":[5,{\"6\":7}]}}}}]"
        };*/

        LogMap _logMap = new LogMap();
        int statementCount = 0;

        LogResult _logResult = null;

        String input = "[0,{\"1\":{\"2\":{\"3\":{\"4\":[5,{\"6\":7}]}}}}]";

        for (int i = 0; i < 5; i++) {

            input = ReverseRegex.readConfigFile().getJson();

            _logResult = new LogResult(_logMap);

            ParserApplication parserApplication = new ParserApplication();
            parserApplication.init(_logResult);
            parserApplication.main(input);

            statementCount += _logResult.getLogList().size();
        }


        ///////////////////////
        String lastInput = "";

        for (int i = 0; i < 5; i++) {

            try {
                List<Object> g = new ArrayList<Object>();
                ExecutorService myExecutor = Executors.newFixedThreadPool(1);
                Future<Pair<String, LogResult>> task;


                task = myExecutor.submit(new Callable<Pair<String, LogResult>>() {
                    public Pair<String, LogResult> call() throws Exception {

                        String input = ReverseRegex.readConfigFile().getJson();

                        LogResult _logResult = new LogResult(_logMap);

                        ParserApplication parser = new ParserApplication();
                        Method initMethod = parser.getClass().getDeclaredMethod(ReverseRegex.myProperties.get("init_function").toString(), LogResult.class);
                        Method invokeMethod = parser.getClass().getDeclaredMethod(ReverseRegex.myProperties.get("execute_function").toString(), String.class);

                        initMethod.invoke(parser, _logResult);
                        invokeMethod.invoke(input);

                        return new Pair(input, _logResult);
                    }
                });

                lastInput = task.get().getKey();
                _logResult = task.get().getValue();

                myExecutor.shutdown();

            } catch (InterruptedException e) {
                //e.printStackTrace();
            } catch (ExecutionException e) {
                //e.printStackTrace();
            }

        }
        ///////////////////////


        System.out.println("Map Size : " + _logMap.size());
        System.out.println("Log Size : " + statementCount);

        // create expected mutation matrix

        // base matrix containing all possibilities
        MutationMatrix mutationMatrix = new MutationMatrix();

        // prevent duplicate attempts to mutate same statement
        HashSet<String> processedSet = new HashSet<>();

        // key : className
        // value : List<<LogRecord, Modifier>>
        HashMap<String, List<Pair<LogRecord, ModifierVisitor<Void>>>> mutationMap = new HashMap<>();

        for (String logRecordKey :
                _logResult.getLogList()) {

            if (processedSet.contains(logRecordKey)) {
                continue;
            }
            // else
            processedSet.add(logRecordKey);
            LogRecord record = _logResult.getRecord(logRecordKey);

            if (record == null) {
                continue;
            }

            String key = record.className + "|" + record.method;

            if (!mutationMap.containsKey(key)) {
                mutationMap.put(key, new ArrayList<>());
            }

            // creating value list item
            for (String applicableMutation :
                    mutationMatrix.getPossibleMutations(record)) {

                Class mutatorClass = Class.forName("org.smaith2.jsonParser.mutator.Ast.Modifiers." + applicableMutation + "Modifier");
                ModifierVisitor<Void> mutator = (ModifierVisitor<Void>) mutatorClass.newInstance();

                mutationMap.get(key).add(new Pair<>(record, mutator));
            }
        }

//        // mutate
//        // TODO : FIX! hard coded. probably needs to be passed in from the Main method
//        List<Pair<LogRecord, ModifierVisitor<Void>>> expressionMutationPairs = new ArrayList<>();
//
//        LogRecord logRecord = new LogRecord();
//        logRecord.className = "org.json.simple.ItemList";
//        logRecord.method = "toString";
//        logRecord.statementNumber = -1;
//        logRecord.construct = "";
//        logRecord.expressionType = "com.github.javaparser.ast.body.VariableDeclarator";
//        logRecord.expression = "sb = new StringBuffer()";
//        logRecord.operator = "";
//
//        expressionMutationPairs.add(new Pair<>(logRecord, new JIDModifier()));
//
//        logRecord = new LogRecord();
//        logRecord.className = "org.json.simple.ItemList";
//        logRecord.method = "toString";
//        logRecord.statementNumber = -1;
//        logRecord.construct = "$For";
//        logRecord.expressionType = "com.github.javaparser.ast.expr.BinaryExpr";
//        logRecord.expression = "i < items.size()";
//        logRecord.operator = "com.github.javaparser.ast.expr.BinaryExpr.Operator.LESS";
//
//        expressionMutationPairs.add(new Pair<>(logRecord, new RORModifier()));
//
//        // TODO : hard coding ended
//
//        SourceModifier2 sourceModifier = new SourceModifier2("org.json.simple.ItemList", expressionMutationPairs);
//        sourceModifier.modify();

        System.out.println("Here");

//        for (String classAndMethod :
//                mutationMap.keySet()) {
//
//            System.out.println("Modifying : " + classAndMethod);
//
//            List<Pair<LogRecord, ModifierVisitor<Void>>> expressionMutationPairs = mutationMap.get(classAndMethod);
//
//            String[] classAndMethodSplit = classAndMethod.split("\\|");
//            SourceModifier2 sourceModifier = new SourceModifier2(classAndMethodSplit[0], classAndMethodSplit[1], expressionMutationPairs);
//            sourceModifier.modify();
//
////            // TODO : only one for now
////            break;
//        }

        try {
            List<Object> g = new ArrayList<Object>();
            ExecutorService myExecutor = Executors.newFixedThreadPool(50);
            Set<Callable<Boolean>> callables = new HashSet<Callable<Boolean>>();

            for (String classAndMethod :
                    mutationMap.keySet()) {

                callables.add(new Callable<Boolean>() {
                    public Boolean call() throws Exception {

                        System.out.println("Modifying : " + classAndMethod);

                        List<Pair<LogRecord, ModifierVisitor<Void>>> expressionMutationPairs = mutationMap.get(classAndMethod);

                        String[] classAndMethodSplit = classAndMethod.split("\\|");
                        SourceModifier2 sourceModifier = new SourceModifier2(classAndMethodSplit[0], classAndMethodSplit[1], expressionMutationPairs);
                        sourceModifier.modify();

                        //            // TODO : only one for now
                        //            break;

                        return true;
                    }
                });
            }


            List<Future<Boolean>> futures = myExecutor.invokeAll(callables);


            myExecutor.shutdown();
        } catch (InterruptedException e) {
            //e.printStackTrace();
        }

        List<CtClass> ctClasses = SourceModifier2.getAllCtClasses();
        for (CtClass ctClass :
                ctClasses) {
            if (ctClass.isFrozen())
                ctClass.defrost();

            try {
                ctClass.toClass();
            } catch (CannotCompileException e) {
                //e.printStackTrace();
            }
        }

        // run mutated code
        LogResult _logResultMutated = new LogResult(_logMap);

        ParserApplication parserApplication = new ParserApplication();

        try {
            parserApplication.init(_logResultMutated);
            parserApplication.main(lastInput); // input is still set from the last call prior to mutations
        } catch (Throwable e) {
            // gobble. we are expecting errors
        }

        // output log comparison
        System.out.println("Original code log length : " + _logResult.getLogList().size());
        System.out.println("Mutated code log length : " + _logResultMutated.getLogList().size());

        outputLogComparison(_logResult, _logResultMutated);

    }

    private static void outputLogComparison(LogResult _logResult, LogResult _logResultMutated) {
        int i = 0;

        System.out.println("\r\n~*~*~*~ Printing instrumentation comparison ~*~*~*~\r \n");

        while (_logResult.getKey(i).equals(_logResultMutated.getKey(i))) {
            System.out.println("Same at idx " + i + " : " + _logResult.getRecord(i).prettyPrint());
            i++;

            if (_logResult.getLogList().size() == i || _logResultMutated.getLogList().size() == i) {
                System.out.println("Mismatch at idx " + i);
                break;
            }
        }

        if (i != _logResultMutated.getLogList().size()) {

            System.out.println("\r\n~*~*~ Mismatch record ~*~*~\r\n");
            System.out.println("Original code instrumentation");
            System.out.println(_logResult.getRecord(i).prettyPrint());

            System.out.println("Mutated code instrumentation");
            System.out.println(_logResultMutated.getRecord(i).prettyPrint());
        } else {
            System.out.println("\r\n~*~*~ Reached end of log for mutated code ~*~*~\r\n");
        }
    }

}


