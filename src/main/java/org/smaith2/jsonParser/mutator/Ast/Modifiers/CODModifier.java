package org.smaith2.jsonParser.mutator.Ast.Modifiers;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.BinaryExpr;
import com.github.javaparser.ast.visitor.ModifierVisitor;

// Conditional operator deletion
public class CODModifier extends ModifierVisitor<Void> {

    @Override
    public Node visit(BinaryExpr node, Void args) {

        BinaryExpr.Operator operator = node.getOperator();
        Node n1=null;
        if(operator == BinaryExpr.Operator.AND || operator == BinaryExpr.Operator.OR)
        {
            n1 = node.getChildNodes().get(0);
        }
        if(n1!=null)
        {
            return n1;
        }

        else return node;
    }
}