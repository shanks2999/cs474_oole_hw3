package org.smaith2.jsonParser.mutator.Ast;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.stmt.*;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import javafx.util.Pair;
import org.smaith2.jsonParser.console.instrumentation.LogRecord;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BlockSelector extends ModifierVisitor<Void> {


//    // key : logRecord
//    // val : modified block
//    // TODO : probably not needed
//    private HashMap<LogResult, String> mutations = new HashMap<>();

    // key : method + constructs
    // val : HashMap : {String:expression, ModifierVisitor<Void>:modifier}
    private HashMap<String, HashMap<String, ModifierVisitor<Void>>> _reverseMutationMap;

    private List<String> _blockStack;
    private String currentMethodDescriptor = "";

    public BlockSelector(List<Pair<LogRecord, ModifierVisitor<Void>>> expressionMutationPairs) {
        _blockStack = new ArrayList<>();

        _reverseMutationMap = new HashMap<>();
        for (Pair<LogRecord, ModifierVisitor<Void>> emPair :
                expressionMutationPairs) {

            LogRecord record = emPair.getKey();
            String innerKey = "{{" + record.methodDescriptor + "}}#" + record.method + ((record.construct == null || record.construct.isEmpty()) ? "" : "." + record.construct);
            if(!_reverseMutationMap.keySet().contains(innerKey)) {
                _reverseMutationMap.put(innerKey, new HashMap<>());
            }

            //
            HashMap<String, ModifierVisitor<Void>> innerHash = _reverseMutationMap.get(innerKey);

            innerHash.put(record.expression, emPair.getValue());
        }

    }

    // expression matching and modification

    @Override
    public Node visit(VariableDeclarator node, Void args) {
        super.visit(node, args);
        tryProcessExpression(node);
        return node;
    }

    @Override
    public Node visit(AssignExpr node, Void args) {
        super.visit(node, args);
        tryProcessExpression(node);
        return node;
    }

    @Override
    public Node visit(BinaryExpr node, Void args) {
        super.visit(node, args);
        tryProcessExpression(node);
        return node;
    }

    @Override
    public Node visit(UnaryExpr node, Void args) {
        super.visit(node, args);
        tryProcessExpression(node);
        return node;
    }

    @Override
    public Node visit(CastExpr node, Void args) {
        super.visit(node, args);
        tryProcessExpression(node);
        return node;
    }

    @Override
    public Node visit(ExplicitConstructorInvocationStmt node, Void args) {
        super.visit(node, args);
        tryProcessExpression(node);
        return node;
    }

    @Override
    public Node visit(SuperExpr node, Void args) {
        super.visit(node, args);
        tryProcessExpression(node);
        return node;
    }


    // block path maintenance
    @Override
    public Node visit(ConstructorDeclaration node, Void args) {
        setCurrentMethodDescriptor(node.getDeclarationAsString(true, true, true));
        pushBlockPath("super#ctor");

        super.visit(node, args);

        popBlockPath();
        unsetCurrentMethodDescriptor();

        return node;
    }

    @Override
    public Node visit(MethodDeclaration node, Void args) {
        setCurrentMethodDescriptor(node.getDeclarationAsString(true, true, true));
        pushBlockPath("#" + node.getName().asString());

        super.visit(node, args);

        popBlockPath();
        unsetCurrentMethodDescriptor();

        return node;
    }

    @Override
    public Node visit(IfStmt node, Void args) {
        pushBlockPath("$If");

        node.getCondition().accept(this, args);

        super.visit(node, args);

        popBlockPath();
        return node;
    }

    @Override
    public Node visit(WhileStmt node, Void args) {
        pushBlockPath("$While");

        node.getCondition().accept(this, args);

        super.visit(node, args);

        popBlockPath();
        return node;
    }

    @Override
    public Node visit(ForStmt node, Void args) {
        pushBlockPath("$For");

        node.getInitialization().accept(this, args);
        if(node.getCompare().isPresent()){
            node.getCompare().get().accept(this, args);
        }
        node.getUpdate().accept(this, args);

        super.visit(node, args);

        popBlockPath();
        return node;
    }

    @Override
    public Node visit(SwitchStmt node, Void args) {
        pushBlockPath("$Switch");

        super.visit(node, args);

        popBlockPath();
        return node;
    }

    // helper fns
    private void pushBlockPath(String block) {
        _blockStack.add(block);
    }

    private void popBlockPath() {
        _blockStack.remove(_blockStack.size() - 1);
    }

    private String getCurrentBlockPath() {
        return String.join(".", _blockStack);
    }

    private void tryProcessExpression(Node node) {

        String blockPath =  "{{" + getCurrentMethodDescriptor() + "}}" + getCurrentBlockPath();
        if(_reverseMutationMap.keySet().contains(blockPath)) {
            HashMap<String, ModifierVisitor<Void>> innerHash = _reverseMutationMap.get(blockPath);

            String innerKey = node.toString();
            if(innerHash.keySet().contains(innerKey)) {
                node.accept(innerHash.get(innerKey), null);
            }
        }
    }

    private void setCurrentMethodDescriptor(String fullMethodName) {
        this.currentMethodDescriptor = fullMethodName;
    }
    private String getCurrentMethodDescriptor() {
        return this.currentMethodDescriptor;
    }
    private String unsetCurrentMethodDescriptor() {
        return this.currentMethodDescriptor;
    }

}


