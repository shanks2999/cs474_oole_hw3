package org.smaith2.jsonParser.mutator.Ast.Modifiers;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.stmt.ExplicitConstructorInvocationStmt;
import com.github.javaparser.ast.visitor.ModifierVisitor;

// Explicit call to parent's constructor deletion
public class IPCModifier extends ModifierVisitor<Void> {

    @Override
    public Node visit(ExplicitConstructorInvocationStmt expr, Void args) {
        expr.remove();
        return expr;
    }
}
