package org.smaith2.jsonParser.mutator.Ast.Modifiers;


import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import java.util.HashMap;

// Arithmetic operator replacement for binary operators (2nd type)
public class AORB2Modifier extends ModifierVisitor<Void> {

    @Override
    public Node visit(BinaryExpr node, Void args)
    {

        BinaryExpr newBin = new BinaryExpr();
        newBin.setLeft(new EnclosedExpr(node));

        IntegerLiteralExpr integerLiteralExpr = new IntegerLiteralExpr(5);
        newBin.setRight(integerLiteralExpr);

        newBin.setOperator(BinaryExpr.Operator.MULTIPLY);

        node.replace(node, newBin);

        return newBin;
    }
}

