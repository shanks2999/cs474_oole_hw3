package org.smaith2.jsonParser.mutator.Ast;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.stmt.Statement;
import com.github.javaparser.ast.visitor.ModifierVisitor;

public abstract class AbstractVisitor extends ModifierVisitor<Void> {

    public int findStatementIndex(com.github.javaparser.ast.NodeList<Statement> statements, Node statement){
        for (int i=0; i < statements.size(); i ++)
        {
            if(statements.get(i).toString().contains(statement.toString())){
                return i;
            }
        }
        return -1;
    }

}
