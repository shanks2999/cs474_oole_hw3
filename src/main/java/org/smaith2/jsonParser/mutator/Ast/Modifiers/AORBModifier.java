package org.smaith2.jsonParser.mutator.Ast.Modifiers;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.BinaryExpr;
import com.github.javaparser.ast.visitor.ModifierVisitor;

import java.util.HashMap;

// Arithmetic operator replacement for binary operators
public class AORBModifier extends ModifierVisitor<Void> {

    HashMap<BinaryExpr.Operator, BinaryExpr.Operator> replacementMap;

    public AORBModifier() {

        replacementMap = new HashMap<>();
        replacementMap.put(BinaryExpr.Operator.PLUS, BinaryExpr.Operator.MINUS);
        replacementMap.put(BinaryExpr.Operator.MINUS, BinaryExpr.Operator.PLUS);
        replacementMap.put(BinaryExpr.Operator.MULTIPLY, BinaryExpr.Operator.DIVIDE);
        replacementMap.put(BinaryExpr.Operator.DIVIDE, BinaryExpr.Operator.MULTIPLY);

    }

    @Override
    public Node visit(BinaryExpr node, Void args) {

        // if binary expression is a logical operator
        // modify existing operator to replacements defined in replacemtMap

        BinaryExpr.Operator operator = node.getOperator();
        if(operator == BinaryExpr.Operator.PLUS)
        {
            node.setOperator(replacementMap.get(operator));
        }
        else if(operator == BinaryExpr.Operator.MINUS)
        {
            node.setOperator(replacementMap.get(operator));
        }
        else if(operator == BinaryExpr.Operator.MULTIPLY)
        {
            node.setOperator(replacementMap.get(operator));
        }
        else if(operator == BinaryExpr.Operator.DIVIDE)
        {
            node.setOperator(replacementMap.get(operator));
        }
        //System.out.println(node.toString());

        return node;
    }
}
