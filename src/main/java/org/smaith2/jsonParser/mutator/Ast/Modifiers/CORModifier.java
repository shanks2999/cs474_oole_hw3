package org.smaith2.jsonParser.mutator.Ast.Modifiers;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.BinaryExpr;
import com.github.javaparser.ast.expr.UnaryExpr;
import com.github.javaparser.ast.visitor.ModifierVisitor;

import java.util.HashMap;

public class CORModifier extends ModifierVisitor<Void> {

    HashMap<BinaryExpr.Operator, BinaryExpr.Operator> replacementMap;

    public CORModifier() {

        replacementMap = new HashMap<>();
        replacementMap.put(BinaryExpr.Operator.AND, BinaryExpr.Operator.OR);
        replacementMap.put(BinaryExpr.Operator.OR, BinaryExpr.Operator.AND);
    }

    @Override
    public Node visit(BinaryExpr node, Void args) {

        // if binary expression is a logical binary expression
            // modify existing operator to replacements defined in replacemtMap

        BinaryExpr.Operator operator = node.getOperator();
        if(operator == BinaryExpr.Operator.AND ||
                operator == BinaryExpr.Operator.OR)
        {
            node.setOperator(replacementMap.get(operator));
        }
        return node;
    }

    @Override
    public Node visit(UnaryExpr node, Void args) {

        // if unary expression is a logical NOT operator
        // replace node with its expression, effectively replacing the NOT

        UnaryExpr.Operator operator = node.getOperator();
        if(operator == UnaryExpr.Operator.LOGICAL_COMPLEMENT)
        {
            node.replace(node.getExpression());
        }
        return node;
    }
}
