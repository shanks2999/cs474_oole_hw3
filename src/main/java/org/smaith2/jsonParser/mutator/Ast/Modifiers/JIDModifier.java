package org.smaith2.jsonParser.mutator.Ast.Modifiers;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.visitor.ModifierVisitor;

// Member variable initialization deletion
public class JIDModifier extends ModifierVisitor<Void> {

    @Override
    public Node visit(VariableDeclarator expr, Void args) {
        expr.removeInitializer();
        return expr;
    }
}
