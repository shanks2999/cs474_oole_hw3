package org.smaith2.jsonParser.mutator.Ast.Modifiers;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.CastExpr;
import com.github.javaparser.ast.visitor.ModifierVisitor;

// Type cast operator deletion
public class PCDModifier extends ModifierVisitor<Void> {

    @Override
    public Node visit(CastExpr expr, Void args) {
        //expr.remove();
        return expr.getExpression();
    }
}
