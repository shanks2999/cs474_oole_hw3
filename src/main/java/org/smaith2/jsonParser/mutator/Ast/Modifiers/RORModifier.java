package org.smaith2.jsonParser.mutator.Ast.Modifiers;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.BinaryExpr;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import com.sun.javafx.fxml.expression.BinaryExpression;
import javafx.beans.binding.BooleanExpression;

import java.util.HashMap;

public class RORModifier extends ModifierVisitor<Void> {

    HashMap<BinaryExpr.Operator, BinaryExpr.Operator> replacementMap;

    public RORModifier() {

        replacementMap = new HashMap<>();
        replacementMap.put(BinaryExpr.Operator.EQUALS, BinaryExpr.Operator.NOT_EQUALS);
        replacementMap.put(BinaryExpr.Operator.NOT_EQUALS, BinaryExpr.Operator.EQUALS);
        replacementMap.put(BinaryExpr.Operator.GREATER, BinaryExpr.Operator.GREATER_EQUALS);
        replacementMap.put(BinaryExpr.Operator.GREATER_EQUALS, BinaryExpr.Operator.GREATER);
        replacementMap.put(BinaryExpr.Operator.LESS, BinaryExpr.Operator.LESS_EQUALS);
        replacementMap.put(BinaryExpr.Operator.LESS_EQUALS, BinaryExpr.Operator.LESS);

    }

    @Override
    public Node visit(BinaryExpr node, Void args) {

        // if binary expression is a logical operator
            // modify existing operator to replacements defined in replacemtMap

        BinaryExpr.Operator operator = node.getOperator();
        if(operator == BinaryExpr.Operator.EQUALS ||
                operator == BinaryExpr.Operator.NOT_EQUALS ||
                operator == BinaryExpr.Operator.GREATER ||
                operator == BinaryExpr.Operator.GREATER_EQUALS ||
                operator == BinaryExpr.Operator.LESS ||
                operator == BinaryExpr.Operator.LESS_EQUALS)
        {
            node.setOperator(replacementMap.get(operator));
        }
        return node;
    }
}
