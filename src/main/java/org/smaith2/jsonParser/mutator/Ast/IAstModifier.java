package org.smaith2.jsonParser.mutator.Ast;

public interface IAstModifier {

    String modify();

}
