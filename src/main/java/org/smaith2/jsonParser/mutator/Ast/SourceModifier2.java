package org.smaith2.jsonParser.mutator.Ast;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.BodyDeclaration;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.TypeDeclaration;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import javafx.util.Pair;
import javassist.*;
import org.smaith2.jsonParser.console.instrumentation.LogRecord;
import org.smaith2.jsonParser.mutator.helper.ClassFileLocator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SourceModifier2 implements IAstModifier {

    private final String _className;
    private final String _methodName;
    private final String _methodDescriptor;
    private List<Pair<LogRecord, ModifierVisitor<Void>>> _expressionMutationPairs;
    private CompilationUnit unit;

    private static ClassPool _staticClassPool;
    private static HashMap<String, CtClass> _classMap;

    static {
        _staticClassPool = new ClassPool(true);
        _staticClassPool.importPackage("org.json.simple.parser");
        _staticClassPool.importPackage("org.json.simple");
        _staticClassPool.importPackage("java.util");

        _classMap = new HashMap<>();
        //pool.insertClassPath("./classes");
    }

    public SourceModifier2(String className, String methodName, List<Pair<LogRecord, ModifierVisitor<Void>>> expressionMutationPairs) throws IOException, ClassNotFoundException {
        _className = className;
        _methodName = methodName;
        _methodDescriptor = expressionMutationPairs.size() > 0 ? expressionMutationPairs.get(0).getKey().methodDescriptor : "";
        _expressionMutationPairs = expressionMutationPairs;

        String sourceFile = ClassFileLocator.getClassPath(className);

        unit = JavaParser.parse(new java.io.File(sourceFile), Charset.forName("UTF-8"));
    }


    @Override
    public String modify() {

        if (this._expressionMutationPairs.size() == 0) {
            return "";
        }

        BlockSelector visitor = new BlockSelector(this._expressionMutationPairs);
        unit.accept(visitor, null);

        //writeOut(this._className, unit.toString());

        String methodBody = "";

        methodBody = getUpdatedMethodBody(unit, this._methodName, this._methodDescriptor);
        //updateByteCode(this._className, this._methodName, methodBody);
        replaceByteCode(this._className, this._methodName, methodBody);

        //System.out.println(unit);

        return "";
    }

    private String getUpdatedMethodBody(CompilationUnit cu, String methodName, String methodDescriptor) {

        String methodBody = "{}";

        NodeList<TypeDeclaration<?>> types = cu.getTypes();
        for (TypeDeclaration<?> type : types) {
            // Go through all fields, methods, etc. in this type
            NodeList<BodyDeclaration<?>> members = type.getMembers();
            for (BodyDeclaration<?> member : members) {
                if (!methodName.equals("ctor") && member instanceof MethodDeclaration) {
                    MethodDeclaration method = (MethodDeclaration) member;

                    if (method.getNameAsString().equals(methodName)
                            && method.getDeclarationAsString(true, true, true).equals(methodDescriptor)) {

                        if (method.getBody().isPresent()) {
//                            methodBody = method.getBody().get().toString();
                            methodBody = method.toString();
                        }
                    }
                }

                // else
                if (methodName.equals("ctor") && member instanceof ConstructorDeclaration) {
                    ConstructorDeclaration ctor = (ConstructorDeclaration) member;

                    if (ctor.getNameAsString().equals(methodName)
                            && ctor.getDeclarationAsString(true, true, true).equals(methodDescriptor)) {
                        methodBody = ctor.toString();
                    }
                }
            }
        }
        return methodBody;
    }

    private synchronized void updateByteCode(String className, String methodName, String methodBody) {

        CtClass ctClass = null;
        try {
            ClassPool pool = new ClassPool(true);
            //pool.insertClassPath("./classes");

            ctClass = pool.get(className);
        } catch (NotFoundException e) {
            //e.printStackTrace();
        }

        if (!methodName.equals("ctor")) {
            CtMethod[] ctMethods = ctClass.getDeclaredMethods();

            boolean isReplaced = true;
            for (int i = 0; i < ctMethods.length; i++) {
                isReplaced = true;
                if (ctMethods[i].getName().equals(methodName)) {
                    try {
                        ctMethods[i].setBody(methodBody);
                    } catch (CannotCompileException e) {
                        System.out.println(methodBody);
                        isReplaced = false;
                        System.out.println("Replace method FAILED: " + methodName);
                        //e.printStackTrace();
                    }
                    // if we got here, we were successful. move on
                    if (isReplaced) {
                        System.out.println("Replaced method : " + methodName);
                        break;
                    }
                }
            }

        } else {
            CtConstructor[] ctors = ctClass.getConstructors();
            boolean isReplaced = true;
            for (int i = 0; i < ctors.length; i++) {
                isReplaced = true;
                if (ctors[i].getName().equals(methodName)) {
                    try {
                        ctors[i].setBody(methodBody);
                    } catch (CannotCompileException e) {
                        isReplaced = false;
                        System.out.println("Replace ctor FAILED: " + methodName + ":" + e);
                        //e.printStackTrace();
                    }
                    // if we got here, we were successful. move on
                    if (isReplaced) {
                        System.out.println("Replaced ctor : " + methodName);
                        break;
                    }
                }
            }

            if(isReplaced) {
                try {
                    if(ctClass.isFrozen()) {
                        ctClass.defrost();
                    }
                    ctClass.writeFile();
                } catch (CannotCompileException e) {
                    System.out.println("Replace ctor FAILED: " + methodName + ":" + e);
                    //e.printStackTrace();
                } catch (IOException e) {
                    System.out.println("Replace ctor FAILED: " + methodName + ":" + e);
                    //e.printStackTrace();
                } catch (NotFoundException e) {
                    System.out.println("Replace ctor FAILED: " + methodName + ":" + e);
                    //e.printStackTrace();
                }
            }
        }
    }

    private synchronized void replaceByteCode(String className, String methodName, String methodBody) {

        CtClass ctClass = getCtClass(className);

        boolean isReplaced = true;
        if (!methodName.equals("ctor")) {
            CtMethod[] ctMethods = ctClass.getDeclaredMethods();

            for (int i = 0; i < ctMethods.length; i++) {
                isReplaced = true;
                if (ctMethods[i].getName().equals(methodName)) {
                    try {
                        ctClass.removeMethod(ctMethods[i]);
                        CtMethod newMethod = CtMethod.make(methodBody, ctClass);
                        ctClass.addMethod(newMethod);
//                        ctMethods[i].setBody(methodBody);
                    } catch (CannotCompileException e) {
                        //System.out.println(methodBody);
                        isReplaced = false;
                        System.out.println("Replace method FAILED: " + methodName);
                        //e.printStackTrace();
                    } catch (NotFoundException e) {
                        //System.out.println(methodBody);
                        isReplaced = false;
                        System.out.println("Replace method FAILED: " + methodName);
                        //e.printStackTrace();
                    }
                    // if we got here, we were successful. move on
                    if (isReplaced) {
                        System.out.println("Replaced method : " + methodName);
                        break;
                    }
                }
            }

        } else {
            CtConstructor[] ctors = ctClass.getConstructors();
            for (int i = 0; i < ctors.length; i++) {
                isReplaced = true;
                if (ctors[i].getName().equals(methodName)) {
                    try {
                        ctClass.removeConstructor(ctors[i]);
                        CtConstructor newMethod = CtNewConstructor.make(methodBody, ctClass);
                        ctClass.addConstructor(newMethod);
                    } catch (CannotCompileException e) {
                        isReplaced = false;
                        System.out.println("Replace ctor FAILED: " + methodName + ":" + e);
//                        e.printStackTrace();
                    } catch (NotFoundException e) {
                        isReplaced = false;
                        System.out.println("Replace ctor FAILED: " + methodName + ":" + e);
//                        e.printStackTrace();
                    }
                    // if we got here, we were successful. move on
                    if (isReplaced) {
                        System.out.println("Replaced ctor : " + methodName);
                        break;
                    }
                }
            }
        }
    }

    private void writeOut(String className, String fileContents) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(Paths.get("./mutatedFiles", className + ".java").toFile().getAbsoluteFile()));
            writer.write(fileContents);

        } catch (IOException e) {
        } finally {
            try {
                if (writer != null)
                    writer.close();
            } catch (IOException e) {
            }
        }
    }

    private CtClass getCtClass(String className){
        if(!_classMap.containsKey(className)) {
            try {
                _classMap.put(className, _staticClassPool.get(className));
                System.out.println("Success writing out class file : " + className);
            } catch (NotFoundException e) {
                System.out.println("Error writing out class file : " + className);
                //e.printStackTrace();
            }
        }
        return _classMap.get(className);
    }

    public static List<CtClass> getAllCtClasses() {
        return new ArrayList<>(_classMap.values());
    }

}
