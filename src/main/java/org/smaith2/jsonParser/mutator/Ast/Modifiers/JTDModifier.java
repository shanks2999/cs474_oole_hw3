package org.smaith2.jsonParser.mutator.Ast.Modifiers;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.AssignExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.FieldAccessExpr;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.visitor.ModifierVisitor;

// this keyword deletion
public class JTDModifier extends ModifierVisitor<Void> {

    @Override
    public Node visit(AssignExpr node, Void args) {

        Expression fae = node.getTarget();
        if(fae.getClass() == FieldAccessExpr.class)
        {
            node.setTarget(new NameExpr(((FieldAccessExpr)fae).getName()));
        }
        return node;
    }
}
