package org.smaith2.jsonParser.mutator.helper;

import java.io.File;
import java.io.IOException;

public class ClassFileLocator {

    public static String getClassPath(String className) throws ClassNotFoundException, IOException {

        Class clasInfo = Class.forName(className);

        String[] paths = className.split("\\.");
        String path = String.join("/", paths) + ".java";
        path = "src/main/java/" + path;

        //final File file = new File(clasInfo.getProtectionDomain().getCodeSource().getLocation().getPath());
        final File file = new File(path);
        return file.getAbsolutePath();
    }

}
