package org.smaith2.jsonParser.mutator.helper;

import org.smaith2.jsonParser.console.instrumentation.LogRecord;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class MutationMatrix {

    private HashMap<String, HashSet> _map;

    public MutationMatrix() {
        _map = new HashMap<>();

        _map.put("com.github.javaparser.ast.expr.BinaryExpr-com.github.javaparser.ast.expr.BinaryExpr.Operator.AND", new HashSet<>(Arrays.asList("COR", "COD")));

        _map.put("com.github.javaparser.ast.expr.BinaryExpr-com.github.javaparser.ast.expr.BinaryExpr.Operator.OR", new HashSet<>(Arrays.asList("COR", "COD")));

        _map.put("com.github.javaparser.ast.expr.BinaryExpr-com.github.javaparser.ast.expr.BinaryExpr.Operator.LESS", new HashSet<>(Arrays.asList("ROR")));

        _map.put("com.github.javaparser.ast.expr.BinaryExpr-com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", new HashSet<>(Arrays.asList("ROR")));

        _map.put("com.github.javaparser.ast.expr.BinaryExpr-com.github.javaparser.ast.expr.BinaryExpr.Operator.GREATER_EQUALS", new HashSet<>(Arrays.asList("ROR")));

        _map.put("com.github.javaparser.ast.expr.BinaryExpr-com.github.javaparser.ast.expr.BinaryExpr.Operator.LESS_EQUALS", new HashSet<>(Arrays.asList("ROR")));

        _map.put("com.github.javaparser.ast.expr.BinaryExpr-com.github.javaparser.ast.expr.BinaryExpr.Operator.NOT_EQUALS", new HashSet<>(Arrays.asList("ROR")));

        _map.put("com.github.javaparser.ast.expr.BinaryExpr-com.github.javaparser.ast.expr.BinaryExpr.Operator.GREATER", new HashSet<>(Arrays.asList("ROR")));

        _map.put("com.github.javaparser.ast.expr.CastExpr", new HashSet<>(Arrays.asList("PCD")));

        _map.put("com.github.javaparser.ast.expr.BinaryExpr-com.github.javaparser.ast.expr.BinaryExpr.Operator.PLUS", new HashSet<>(Arrays.asList("AORB", "AORB2")));

        _map.put("com.github.javaparser.ast.expr.BinaryExpr-com.github.javaparser.ast.expr.BinaryExpr.Operator.MINUS", new HashSet<>(Arrays.asList("AORB", "AORB2")));

        _map.put("com.github.javaparser.ast.expr.BinaryExpr-com.github.javaparser.ast.expr.BinaryExpr.Operator.DIVIDE", new HashSet<>(Arrays.asList("AORB", "AORB2")));

        _map.put("com.github.javaparser.ast.expr.BinaryExpr-com.github.javaparser.ast.expr.BinaryExpr.Operator.MULTIPLY", new HashSet<>(Arrays.asList("AORB", "AORB2")));

        _map.put("com.github.javaparser.ast.expr.UnaryExpr-com.github.javaparser.ast.expr.UnaryExpr.Operator.POSTFIX_INCREMENT", new HashSet<>(Arrays.asList("AORB", "AORB2")));

        _map.put("com.github.javaparser.ast.expr.UnaryExpr-com.github.javaparser.ast.expr.UnaryExpr.Operator.PRE_INCREMENT", new HashSet<>(Arrays.asList("AORB", "AORB2")));

        _map.put("com.github.javaparser.ast.expr.UnaryExpr-com.github.javaparser.ast.expr.UnaryExpr.Operator.MINUS", new HashSet<>(Arrays.asList("AORB", "AORB2")));

        _map.put("com.github.javaparser.ast.expr.UnaryExpr-com.github.javaparser.ast.expr.UnaryExpr.Operator.PLUS", new HashSet<>(Arrays.asList("AORB", "AORB2")));

        _map.put("com.github.javaparser.ast.body.VariableDeclarator", new HashSet<>(Arrays.asList("JID")));

        _map.put("com.github.javaparser.ast.stmt.ExplicitConstructorInvocationStmt", new HashSet<>(Arrays.asList("IPC")));

        _map.put("this", new HashSet<>(Arrays.asList("JDT")));
    }

    public HashSet<String> getPossibleMutations(String expressionType, String operator) {

        String key = expressionType + ((operator != null && operator != "") ? "-" + operator : "");

        if(_map.containsKey(key)) {
            return _map.get(key);
        }

        // else
        return new HashSet<>();
    }

    public HashSet<String> getPossibleMutations(LogRecord logRecord) {
        return getPossibleMutations(logRecord.expressionType, logRecord.operator);
    }
}
