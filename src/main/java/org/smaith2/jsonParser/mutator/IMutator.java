package org.smaith2.jsonParser.mutator;

public interface IMutator {

    void mutate(String methodName, int lineNumber);

}
