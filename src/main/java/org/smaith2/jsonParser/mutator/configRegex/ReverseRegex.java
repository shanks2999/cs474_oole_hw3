package org.smaith2.jsonParser.mutator.configRegex;

import com.mifmif.common.regex.Generex;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ReverseRegex {
    public static Properties myProperties;

    static {
        try {
            File configFile = new File("./resources/config.properties");
            FileReader reader = new FileReader(configFile);
            myProperties = new Properties();
            myProperties.load(reader);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static OutputRegex readConfigFile() {
        OutputRegex output = new OutputRegex();
        try {
            int single_size = Integer.parseInt(myProperties.getProperty("single_size"));
            if (myProperties.getProperty("input_category").equals("single")) {
                if (myProperties.getProperty("input_type").equals("string")) {
                    if (myProperties.getProperty("is_unicode").equals("true"))
                        output.setSingle_string(generateRandomUnicodeString(single_size));
                    else
                        output.setSingle_string(generateRandomNonUnicodeString(single_size));
                }
                if (myProperties.getProperty("input_type").equals("integer")) {
                    output.setSingle_number(generateRandomInteger(single_size));
                }
            }

            else if (myProperties.getProperty("input_category").equals("array")) {
                int array_size = Integer.parseInt(myProperties.getProperty("array_size"));

                if (myProperties.getProperty("input_type").equals("string")) {
                    String[] array_string = new String[array_size];
                    for (int i = 0; i < array_size; i++) {
                        if (myProperties.getProperty("is_unicode").equals("true"))
                            array_string[i] = generateRandomUnicodeString(single_size);
                        else
                            array_string[i] = generateRandomNonUnicodeString(single_size);
                    }
                    output.setArray_string(array_string);
                }
                if (myProperties.getProperty("input_type").equals("integer")) {
                    int[] array_number = new int[array_size];
                    for (int i = 0; i < array_size; i++) {
                        array_number[i] = generateRandomInteger(single_size);
                    }
                    output.setArray_number(array_number);
                }
            }

            else if (myProperties.getProperty("input_category").equals("json")) {
                int depth = generateRandomInteger(1);
//                System.out.println(compilationUnit.toString());
                output.setJson(generateDynamicJSON(single_size, depth));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output;
    }

    static String generateDynamicJSON(int single_size, int depth) {
        if(depth == 0) {
            int count = generateRandomInteger(1);
            String items = "[";
            for (int i = 0; i < count; i++) {
                items += "\"" + generateRandomNonUnicodeString(single_size) + "\",";
            }
            items = items.substring(0, items.length() - 1) + "]";
            return items;
        }
        return "{\n\"" + generateRandomNonUnicodeString(generateRandomInteger(1)) + "\":" +
                generateDynamicJSON(single_size,depth-1) + "\n}";
    }

    static int generateRandomInteger(int single_size) {
        Generex generex = new Generex("[1-9][0-9]{" + String.valueOf(single_size - 1) + "}");
        return Integer.parseInt(generex.random());
    }

    static String generateRandomNonUnicodeString(int single_size) {
        Generex generex = new Generex("[a-z]{" + String.valueOf(single_size) + "}");
        return generex.random();
    }

    static String generateRandomUnicodeString(int single_size) {
        Generex generex = new Generex("[a-zA-Z0-9\u0080-\u9fff]{8}");
        return generex.random();
    }
/*
    static String generateRandomUnicodeString(int single_size) {
        Random rand = new Random();
        StringBuffer sb = new StringBuffer(single_size);
        int min = (char) ('\u0000');
        int max = (char) ('\u0AAA');
        while (sb.length() < 10) {
            int randomNum = rand.nextInt((max - min) + 1) + min;
            char c = (char) (randomNum);
            if (Character.isDefined(c)) {
                sb.append(c);
            }
        }
        return sb.toString();
    }
    */
}
