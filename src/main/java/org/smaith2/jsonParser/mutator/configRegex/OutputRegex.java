package org.smaith2.jsonParser.mutator.configRegex;

public class OutputRegex {

    private int single_number;
    private String single_string;
    private int[] array_number;
    private String[] array_string;
    private String json;
    private String init_function;
    private String execute_function;

    public int getSingle_number() {
        return single_number;
    }

    public void setSingle_number(int single_number) {
        this.single_number = single_number;
    }

    public String getSingle_string() {
        return single_string;
    }

    public void setSingle_string(String single_string) {
        this.single_string = single_string;
    }

    public int[] getArray_number() {
        return array_number;
    }

    public void setArray_number(int[] array_number) {
        this.array_number = array_number;
    }

    public String[] getArray_string() {
        return array_string;
    }

    public void setArray_string(String[] array_string) {
        this.array_string = array_string;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public String getInit_function() {
        return init_function;
    }

    public void setInit_function(String init_function) {
        this.init_function = init_function;
    }

    public String getExecute_function() {
        return execute_function;
    }

    public void setExecute_function(String execute_function) {
        this.execute_function = execute_function;
    }
}
