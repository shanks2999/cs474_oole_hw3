package org.smaith2.jsonParser.astConsole.helpers;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.BinaryExpr;
import com.github.javaparser.ast.expr.UnaryExpr;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import javafx.util.Pair;
import org.smaith2.jsonParser.console.instrumentation.Triple;

import java.util.*;

public class NameExpressionCollector extends ModifierVisitor<Void> {

    List<Triple<String, String, String>> expressions;
    public NameExpressionCollector()
    {
        super();
        expressions =  new ArrayList<>();
    }

    @Override
    public Node visit(BinaryExpr expr, Void args) {
        super.visit(expr, args);

        Triple<String, String, String> pair = new Triple<>(expr.getClass().getCanonicalName(), expr.toString(), expr.getOperator().getClass().getCanonicalName() + "." + expr.getOperator().toString());
        expressions.add(pair);
        return expr;
    }

    @Override
    public Node visit(UnaryExpr expr, Void args) {
        super.visit(expr, args);

        Triple<String, String, String> pair = new Triple<>(expr.getClass().getCanonicalName(), expr.toString(), expr.getOperator().getClass().getCanonicalName() + "." + expr.getOperator().toString());
        expressions.add(pair);
        return expr;
    }

    public List<Triple<String, String, String>> getExpressions() {
        return expressions;
    }
}
