package org.smaith2.jsonParser.astConsole.helpers;

import com.github.javaparser.JavaParser;
import com.github.javaparser.Position;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.stmt.*;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import com.github.javaparser.ast.visitor.Visitable;
import org.smaith2.jsonParser.console.instrumentation.Triple;

import java.util.*;

public class CustomVisitor extends ModifierVisitor<Void> {

    private Random r = new Random();
    private List<String> nodePath;
    private String currentMethodDescriptor = "";

    public CustomVisitor() {
        nodePath = new ArrayList<>();
    }

    @Override
    public Visitable visit(VariableDeclarator declarator, Void args) {
        super.visit(declarator, args);

        declarator
                .getAncestorOfType(BlockStmt.class)
                .ifPresent(block -> {

                    // lhs
                    int lineNumber = block.getRange().get().begin.line;
                    int i = 0;
                    Optional<Position> p = declarator.getBegin();
                    if (p.isPresent()) {
                        i = p.get().line;
                    }

                    //Statement instrumentation = getInstrumentation(getnodePath(), i, declarator.getNameAsString());
                    Statement instrumentation = getInstrumextationExpression(getClassName(), getMethodName(), i, getConstructNames(), declarator.getClass().getCanonicalName(), declarator.toString(), "");

                    int idx = findStatementIndex(block.getStatements(), declarator);
                    block.addStatement(idx + 1, instrumentation);

                    // rhs
                    Optional<Expression> initializer = declarator.getInitializer();
                    NameExpressionCollector nameCollector = new NameExpressionCollector();
                    if (initializer.isPresent()) {
                        initializer.get().accept(nameCollector, null);
                    }

                    // TODO : instrument RHS
                    if (nameCollector.getExpressions().size() > 0) {

                        for (Triple<String, String, String> expression :
                                nameCollector.getExpressions()
                                ) {
                            instrumentation = getInstrumextationExpression(getClassName(), getMethodName(), i, getConstructNames(), expression.first, expression.second, expression.third);
                            block.addStatement(idx + 1, instrumentation);
                        }

                    }

                });

        return declarator;
    }

    @Override
    public Node visit(AssignExpr assignment, Void args) {
        super.visit(assignment, args);

        assignment
                .getAncestorOfType(BlockStmt.class)
                .ifPresent(parent -> {

                    // lhs
                    int lineNumber = assignment.getRange().get().begin.line;
                    int i = 0;
                    Optional<Position> p = assignment.getBegin();
                    if (p.isPresent()) {
                        i = p.get().line;
                    }

                    int idx = findStatementIndex(parent.getStatements(), assignment);

                    NameExpressionCollector nameCollector = new NameExpressionCollector();
                    assignment.getTarget().accept(nameCollector, args);
                    assignment.getValue().accept(nameCollector, args);

                    if (nameCollector.getExpressions().size() > 0) {
                        for (Triple<String, String, String> expression :
                                nameCollector.getExpressions()
                                ) {
                            Statement instrumentation = getInstrumextationExpression(getClassName(), getMethodName(), i, getConstructNames(), expression.first, expression.second, expression.third);
                            parent.addStatement(idx + 1, instrumentation);
                        }
                    } else {
                        Statement instrumentation = getInstrumextationExpression(getClassName(), getMethodName(), i, getConstructNames(), assignment.getClass().getCanonicalName(), assignment.toString(), "");
                        parent.addStatement(idx + 1, instrumentation);
                    }
                });

        return assignment;
    }

    @Override
    public Node visit(BinaryExpr expr, Void args) {
        super.visit(expr, args);

        expr
                .getAncestorOfType(BlockStmt.class)
                .ifPresent(parent -> {

                    // lhs
                    int lineNumber = expr.getRange().get().begin.line;
                    int i = 0;
                    Optional<Position> p = expr.getBegin();
                    if (p.isPresent()) {
                        i = p.get().line;
                    }

                    int idx = findStatementIndex(parent.getStatements(), expr);

                    Statement instrumentation = getInstrumextationExpression(getClassName(), getMethodName(), i, getConstructNames(), expr.getClass().getCanonicalName(), expr.toString(), expr.getOperator().getClass().getCanonicalName() + "." + expr.getOperator().toString());
                    insertInstrumentation(expr, parent, idx, instrumentation);
                });

        return expr;
    }

    @Override
    public Node visit(UnaryExpr expr, Void args) {
        super.visit(expr, args);

        expr
                .getAncestorOfType(BlockStmt.class)
                .ifPresent(parent -> {

                    // lhs
                    int lineNumber = expr.getRange().get().begin.line;
                    int i = 0;
                    Optional<Position> p = expr.getBegin();
                    if (p.isPresent()) {
                        i = p.get().line;
                    }

                    int idx = findStatementIndex(parent.getStatements(), expr);

                    Statement instrumentation = getInstrumextationExpression(getClassName(), getMethodName(), i, getConstructNames(), expr.getClass().getCanonicalName(), expr.toString(), expr.getOperator().getClass().getCanonicalName() + "." + expr.getOperator().toString());
                    insertInstrumentation(expr, parent, idx, instrumentation);
                });

        return expr;
    }

    @Override
    public Node visit(CastExpr expr, Void args) {
        super.visit(expr, args);

        expr
                .getAncestorOfType(BlockStmt.class)
                .ifPresent(parent -> {

                    int i = 0;
                    Optional<Position> p = expr.getBegin();
                    if (p.isPresent()) {
                        i = p.get().line;
                    }

                    int idx = findStatementIndex(parent.getStatements(), expr);

                    Statement instrumentation = getInstrumextationExpression(getClassName(), getMethodName(), i, getConstructNames(), expr.getClass().getCanonicalName(), expr.toString(), expr.getClass().getCanonicalName());
                    insertInstrumentation(expr, parent, idx, instrumentation);
                });
        return expr;
    }

    @Override
    public Node visit(ExplicitConstructorInvocationStmt expr, Void args) {
        super.visit(expr, args);

        if(!expr.toString().startsWith("super"))
        {
            return expr;
        }

        // else
        // we only care about calls to versions of super() ctor

        expr
                .getAncestorOfType(BlockStmt.class)
                .ifPresent(parent -> {

                    int i = 0;
                    Optional<Position> p = expr.getBegin();
                    if (p.isPresent()) {
                        i = p.get().line;
                    }

                    int idx = findStatementIndex(parent.getStatements(), expr);

                    Statement instrumentation = getInstrumextationExpression(getClassName(), getMethodName(), i, getConstructNames(), expr.getClass().getCanonicalName(), expr.toString(), "super#ctor");
                    insertInstrumentation(expr, parent, idx, instrumentation);

                });

        return expr;
    }

    @Override
    public Node visit(SuperExpr expr, Void args) {
        super.visit(expr, args);

        expr
                .getAncestorOfType(BlockStmt.class)
                .ifPresent(parent -> {

                    int i = 0;
                    Optional<Position> p = expr.getBegin();
                    if (p.isPresent()) {
                        i = p.get().line;
                    }

                    int idx = findStatementIndex(parent.getStatements(), expr);

                    Statement instrumentation = getInstrumextationExpression(getClassName(), getMethodName(), i, getConstructNames(), expr.getClass().getCanonicalName(), expr.toString(), "super#property");
                    insertInstrumentation(expr, parent, idx, instrumentation);

                });

        return expr;
    }

    @Override
    public Visitable visit(ClassOrInterfaceDeclaration node, Void arg) {

        pushPathSegment(node.getNameAsString());
        super.visit(node, arg);
        popPathSegment();

        return node;
    }

    @Override
    public Visitable visit(PackageDeclaration node, Void arg) {
        pushPathSegment(node.getNameAsString());
        super.visit(node, arg);

        return node;
    }

    @Override
    public Visitable visit(ConstructorDeclaration node, Void arg) {
        setCurrentMethodDescriptor(node.getDeclarationAsString(true, true, true));

        pushPathSegment("#ctor");
        super.visit(node, arg);
        popPathSegment();

        unsetCurrentMethodDescriptor();
        return node;
    }

    @Override
    public Visitable visit(MethodDeclaration node, Void arg) {
        setCurrentMethodDescriptor(node.getDeclarationAsString(true, true, true));

        pushPathSegment("#" + node.getNameAsString());
        NodeList nodeList = node.getNodeLists().get(0);

        for (int i = 0; i < nodeList.size(); i++) {
            //Statement instrumentation = getInstrumentation(getnodePath(), node.getRange().get().begin.line, ((Parameter)nodeList.get(i)).getNameAsString());
            /*Statement instrumentation = getInstrumextationExpression(getClassName(), getMethodName(), i, getConstructNames(), "MethodDeclaration", node.toString());

            if(node.getBody().isPresent()) {
                node.getBody().get().getStatements().add(0, instrumentation);
            }*/
        }

        super.visit(node, arg);

        unsetCurrentMethodDescriptor();
        popPathSegment();

        return node;
    }

    @Override
    public Node visit(ForStmt node, Void args) {
        pushPathSegment("$For");

        NameExpressionCollector nameCollector = new NameExpressionCollector();
        node.getInitialization().accept(nameCollector, null);

        Optional<Expression> compare = node.getCompare();
        if (compare.isPresent()) {
            compare.get().accept(nameCollector, null);
        }
        node.getUpdate().accept(nameCollector, null);

//        node.getAncestorOfType(BlockStmt.class)
//                .ifPresent(block -> {
//                    int lineNumber =  node.getRange().get().begin.line;
//                    int i=0;
//                    Optional<Position> p=node.getBegin();
//                    if(p.isPresent()) {
//                        i = p.get().line;
//                    }
//                    int idx = findStatementIndex(block.getStatements(), node);
//
//                    /*if (nameCollector.getNames().size() > 0) {
//                        Statement instrumentation = getExpressionInstrumentation(peekPath(), i, nameCollector.getNames());
//                        block.addStatement(idx + 1, instrumentation);
//                    }*/
//                });

        Statement orb = node.getBody();
        BlockStmt block;
        if (orb.getClass() != BlockStmt.class) {
            Statement st = orb;
            BlockStmt blkSt = new BlockStmt();
            blkSt.addStatement(1, orb);
            node.setBody(blkSt);
        }
        block = (BlockStmt) node.getBody();

        int lineNumber = node.getRange().get().begin.line;
        int i = 0;
        Optional<Position> p = node.getBegin();
        if (p.isPresent()) {
            i = p.get().line;
        }
        if (nameCollector.getExpressions().size() > 0) {
            for (Triple<String, String, String> expression :
                    nameCollector.getExpressions()
                    ) {
                Statement instrumentation = getInstrumextationExpression(getClassName(), getMethodName(), i, getConstructNames(), expression.first, expression.second, expression.third);

                BlockStmt b;
                b = new BlockStmt();
                b.addStatement(node.getBody());
                b.addStatement(0, instrumentation);
                node.setBody(b);
            }
        }


        // process body with CustomVisitor
        node.getBody().accept(this, args);

        popPathSegment();
        return node;
    }

    @Override
    public Node visit(WhileStmt node, Void args) {
        pushPathSegment("$While");
        // process header items
        NameExpressionCollector nameCollector = new NameExpressionCollector();
        node.getCondition().accept(nameCollector, null);

        node.getAncestorOfType(BlockStmt.class)
                .ifPresent(block -> {
                    int i = node.getRange().get().begin.line;
                    int idx = findStatementIndex(block.getStatements(), node);

                    if (nameCollector.getExpressions().size() > 0) {
                        for (Triple<String, String, String> expression :
                                nameCollector.getExpressions()
                                ) {
                            Statement instrumentation = getInstrumextationExpression(getClassName(), getMethodName(), i, getConstructNames(), expression.first, expression.second, expression.third);
//
                            block.addStatement(idx + 1, instrumentation);
                        }
                    }
                });

        // process body with CustomVisitor
        node.getBody().accept(this, args);

        popPathSegment();
        return node;
    }

    @Override
    public Node visit(IfStmt node, Void args) {
        pushPathSegment("$If");
        // process header items
        NameExpressionCollector nameCollector = new NameExpressionCollector();
        node.getCondition().accept(nameCollector, null);

        node.getAncestorOfType(BlockStmt.class)
                .ifPresent(block -> {
                    int lineNumber = node.getRange().get().begin.line;
                    int i = 0;
                    Optional<Position> p = node.getBegin();
                    if (p.isPresent()) {
                        i = p.get().line;
                    }

                    if (node.getThenStmt().getClass() != BlockStmt.class) {
                        Statement st = node.getThenStmt();
                        BlockStmt newBlk = new BlockStmt();
                        newBlk.addStatement(0, st);
                        node.setThenStmt(newBlk);
                    }

                    if (node.getElseStmt().isPresent()) {
                        if (node.getElseStmt().get().getClass() != BlockStmt.class) {
                            Statement st = node.getElseStmt().get();
                            BlockStmt newBlk = new BlockStmt();
                            newBlk.addStatement(0, st);
                            node.setElseStmt(newBlk);
                        }
                    }

                    int idx = findStatementIndex(((BlockStmt) node.getThenStmt()).getStatements(), node);

                    if (nameCollector.getExpressions().size() > 0) {
                        for (Triple<String, String, String> expression :
                                nameCollector.getExpressions()
                                ) {
                            Statement instrumentation = getInstrumextationExpression(getClassName(), getMethodName(), i, getConstructNames(), expression.first, expression.second, expression.third);
//
                            BlockStmt blk = (BlockStmt) node.getThenStmt();
                            blk.addStatement(idx + 1, instrumentation);
                        }
                    }
                });

        // process body with CustomVisitor
        if (node.hasThenBlock())
            node.getThenStmt().accept(this, args);
        if (node.hasElseBlock())
            node.getElseStmt().get().accept(this, args);

        popPathSegment();
        return node;
    }

    @Override
    public Node visit(SwitchStmt node, Void args) {
        pushPathSegment("$Switch");
        // process header items
        NameExpressionCollector nameCollector = new NameExpressionCollector();
        node.getSelector().accept(nameCollector, null);

        // process body with CustomVisitor


        popPathSegment();
        return node;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////

    private Statement getInstrumextationExpression(String className, String method, int statementNo, String construct, String expressionType, String expression, String operator) {

        className = "\"" + className + "\"";
        method = "\"" + method + "\"";
        construct = "\"" + construct + "\"";
        expressionType = "\"" + expressionType + "\"";
        expression = "\"" + expression + "\"";
        operator = "\"" + operator + "\"";

        // from global constant. !HACK!
        String methodDesciptor = getCurrentMethodDescriptor();
        methodDesciptor = "\"" + methodDesciptor + "\"";


        String strTemplate = "org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum(%s, %s, %d, %s, %s, %s, %s, %s);";
        String strStatement = String.format(strTemplate, className, method, statementNo, construct, expressionType, expression, operator, methodDesciptor);

        //System.out.println(strStatement);
        return JavaParser.parseStatement(strStatement);
    }

    private int findStatementIndex(com.github.javaparser.ast.NodeList<Statement> statements, Node statement) {
        for (int i = 0; i < statements.size(); i++) {
            if (statements.get(i).toString().contains(statement.toString())) {
                return i;
            }
        }
        return -1;
    }

    private void pushPathSegment(String node) {
        nodePath.add(node);
    }

    private void popPathSegment() {
        nodePath.remove(nodePath.size() - 1);
    }

    private void insertInstrumentation(Node expr, BlockStmt parent, int idx, Statement instrumentation) {
        Optional<ThrowStmt> parentThrow = expr.getAncestorOfType(ThrowStmt.class);
        Optional<ReturnStmt> parentReturn = expr.getAncestorOfType(ReturnStmt.class);
        if (parentReturn.isPresent() || parentThrow.isPresent()) {
            if (idx == 0) {
                parent.addStatement(idx + 1, parent.getStatement(idx));
                parent.setStatement(idx, instrumentation);
            } else {
                parent.setStatement(idx - 1, instrumentation);
            }
        } else {
            parent.addStatement(idx + 1, instrumentation);
        }
    }

    private String getClassName() {
        List<String> path = new ArrayList<>();
        for (String node : nodePath) {

            if (!node.startsWith("#") /* method */
                    && !node.startsWith("$") /* construct */) {
                path.add(node);
            }
        }
        return String.join(".", path);
    }

    private String getMethodName() {
        String path = "";
        for (String node : nodePath) {

            if (node.startsWith("#") /* method */)
                return node.replace("#", "");
        }

        return path;
    }

    private String getConstructNames() {
        List<String> path = new ArrayList<>();
        for (String node : nodePath) {

            if (node.startsWith("$"))
                path.add(node);
        }
        return String.join(".", path);
    }

    private void setCurrentMethodDescriptor(String fullMethodName) {
        this.currentMethodDescriptor = fullMethodName;
    }
    private String getCurrentMethodDescriptor() {
        return this.currentMethodDescriptor;
    }
    private String unsetCurrentMethodDescriptor() {
        return this.currentMethodDescriptor;
    }
}
