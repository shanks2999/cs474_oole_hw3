package org.smaith2.jsonParser.astConsole;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseException;
import com.github.javaparser.ast.CompilationUnit;
import org.smaith2.jsonParser.astConsole.helpers.CustomVisitor;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class AstApplication {

    private static String BASE_SOURCE_DIR = "./src/";
    private static String BASE_BACKUP_DIR = "./backup/";

    public static void main(String[] args) throws ParseException {

        try {

            processFiles(BASE_SOURCE_DIR);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void processFiles(String basePath) throws IOException {
        File file = new File(basePath);
        File[] files = new File[0];

        try {
            files = file.listFiles();
        } catch (NullPointerException ex) {
            System.out.println("Error getting file and folder list. Please try again later");
        }

        if(files == null) {
            return;
        }

        for (int i = 0; i < files.length; i ++) {

            File nextFile = files[i];
            if(nextFile.isFile()
                    && !nextFile.getPath().contains("astConsole")
                    && !nextFile.getPath().contains("test")
                    && !nextFile.getPath().contains("instrumentation")
                    && !nextFile.getPath().contains("console")
                    && nextFile.getPath().toLowerCase().endsWith(".java")) {

                System.out.println("Processing file : " + nextFile.getAbsolutePath());
                backupFile(nextFile);
                String newContent = parseFile(nextFile);
                saveUpdatedFile(nextFile, newContent);

            } else {
                processFiles(nextFile.getPath().toString());
            }

            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static void saveUpdatedFile(File file, String content) throws IOException {
        Files.write(file.toPath(), content.getBytes("UTF-8"), StandardOpenOption.CREATE);
    }

    private static void backupFile(File sourceFile) throws IOException {

        File destFile = new File(Paths.get(BASE_BACKUP_DIR, sourceFile.getPath()).toString());
        destFile.getParentFile().mkdirs();
        destFile.createNewFile();
    }

    private static String parseFile(File sourceFile) throws FileNotFoundException {

        CompilationUnit unit = null;
        unit = JavaParser.parse(sourceFile, Charset.forName("UTF-8"));
        unit.accept(new CustomVisitor(), null);

//            Files.write(Paths.get(BASE_SOURCE_DIR, "NewClass.java"), unit.toString().getBytes("UTF-8"), StandardOpenOption.CREATE);

        return unit.toString();
    }
}