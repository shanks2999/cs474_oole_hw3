package org.smaith2.jsonParser.console;
import org.json.simple.JSONValue;
import org.smaith2.jsonParser.console.instrumentation.LogMap;
import org.smaith2.jsonParser.console.instrumentation.LogResult;
import org.smaith2.jsonParser.console.instrumentation.TemplateClass;

import static java.lang.Thread.*;

public class ParserApplication {

    private LogResult _logResult;

    private void init() {
        init(new LogResult(new LogMap()));
    }
    public void init(LogResult logResult) {
        _logResult = logResult;
        TemplateClass.init(_logResult);
    }
    public LogResult getLogResult()
    {
        return _logResult;
    }

    public void main(String jsonInput){

        Object output = JSONValue.parse(jsonInput);
        System.out.println("Input String : " + jsonInput);
        System.out.print("Output");
        System.out.println( output);
        System.out.println("");
    }

}
