package org.smaith2.jsonParser.console;

import org.smaith2.jsonParser.console.instrumentation.LogMap;
import org.smaith2.jsonParser.console.instrumentation.LogRecord;
import org.smaith2.jsonParser.console.instrumentation.LogResult;

public class ParserRunner {

    public static void main(String[] args) {

        String [] parameters = new String[]{
                "{}",
                "{\"2\":{\"3\":{\"4\":[5,{\"6\":7}]}}}",
                "[5,]",
                "[5,,2]",
                "\"[\\\"hello\\\\bworld\\\\\\\"abc\\\\tdef\\\\\\\\ghi\\\\rjkl\\\\n123\\\\u4e2d\\\"]\"",
                "[\"hello\\bworld\\\"abc\\tdef\\\\ghi\\rjkl\\n123\\u4e2d\"]",
                "[0,{\"1\":{\"2\":{\"3\":{\"4\":[5,{\"6\":7}]}}}}]"
        };

        LogMap _logMap = new LogMap();
        int statementCount = 0;

        LogResult _logResult = null;

        for (String input :
                parameters) {

            _logResult = new LogResult(_logMap);

            ParserApplication parserApplication = new ParserApplication();
            parserApplication.init(_logResult);
            parserApplication.main(input);

            statementCount += _logResult.getLogList().size();
        }

        System.out.println("Map Size : " + _logMap.size());
        System.out.println("Log Size : " + statementCount);

        for(int i = 0; i < _logResult.getLogList().size() - 1; i ++) {
            LogRecord item = _logResult.getRecord(i);
            System.out.println(String.format("\"%s\",\"%s\",\"%d\",\"%s\",\"%s\",\"%s\", \"%s\"", item.className, item.method, item.statementNumber, item.construct, item.expressionType, item.expression, item.operator));
        }
    }
}
