package org.smaith2.jsonParser.console.instrumentation;

import java.util.ArrayList;

public class LogResult {

    private ArrayList<String> _logList;
    private LogMap _logMap;

    /* constructors */
    public LogResult(LogMap logMap) {
        _logList = new ArrayList<>();
        _logMap = logMap;
    }

    /* public methods */

    public void addLog(LogRecord record) {
        String key = this._logMap.put(record);
        _logList.add(key);
    }

    public String getKey(int index) {
        return _logList.get(index);
    }

    public LogRecord getRecord(int index) {
        return _logMap.get(_logList.get(index));
    }

    public LogRecord getRecord(String key) {
        return _logMap.get(key);
    }

    public LogMap getLogMap() {
        return _logMap;
    }
    public ArrayList<String> getLogList() {
        if(_logList == null) {
            return new ArrayList<>();
        }

        return _logList;
    }
}

