package org.smaith2.jsonParser.console.instrumentation;

import javafx.util.Pair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TemplateClass {

//    public static void instrum(int lineNumber, String construct, String obj){
//        System.out.println(String.format("L : %d || %s || %s", lineNumber, construct, obj));
//    }
//
//    public static void instrum(int lineNumber, String expression, Pair<String, Object> ... dataPoints) {
//
//        String tupleTemplate = " %s : %s"; // name (string value), type (%s, %d)
//        List<String> tupleConcat = new ArrayList<>();
//        List<Object> values = new ArrayList<>();
//
//        // initializing params
//        values.add(lineNumber);
//        values.add(expression);
//
//        for (Pair<String, Object> dataPoint : dataPoints) {
//            String temp = String.format(tupleTemplate, dataPoint.getKey(), dataPoint.getValue() instanceof Integer ? "%d" : "%s ");
//            tupleConcat.add(temp);
//            values.add(dataPoint.getValue());
//        }
//
//        String loggingTemplate = "L : %d || %s ||";
//        loggingTemplate += String.join(" || ", tupleConcat);
//
//        System.out.println(String.format(loggingTemplate, values.toArray()));
//    }

    /* new logging */
    private static LogResult _logResult;
    public static void init(LogResult logResult) {
        _logResult = logResult;
    }

    public static void instrum(String className, String method, int statementNo, String construct, String expressionType, String expression, String operator, String methodDescriptor) {

        LogRecord record = new LogRecord();

        record.className = className;
        record.method = method;
        record.statementNumber = statementNo;
        record.construct = construct;
        record.expressionType = expressionType;
        record.expression = expression;
        record.operator = operator;
        record.methodDescriptor = methodDescriptor;

        _logResult.addLog(record);
    }

    public static LogResult getLogResult() {
        return _logResult;
    }
}
