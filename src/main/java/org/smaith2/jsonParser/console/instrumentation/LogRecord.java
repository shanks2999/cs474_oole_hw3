package org.smaith2.jsonParser.console.instrumentation;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LogRecord {

    private static final MessageDigest digestProvider = getDigestProvider();
    private static MessageDigest getDigestProvider() {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            // gobble exception
        }
        return md;
    }

    public String className;
    public String method;
    public int statementNumber;
    public String construct;
    public String expressionType;
    public String expression;
    public String operator;
    public String methodDescriptor;

    public String getKey(){
        if(digestProvider != null) {
            byte[] digest = new byte[0];
            try {
                digest = digestProvider.digest(this.getString().getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return new String(digest, StandardCharsets.UTF_8);
        } else
        {
            return getString();
        }
    }
    private String getString(){
        return className + "|" + method + "|" + statementNumber + "|" + construct + "|" + expressionType + "|" + expression + "|" + operator + "|" + methodDescriptor;
    }

    public String prettyPrint() {
        return String.join(" | ", getString().split("\\|"));
    }

}
