package org.smaith2.jsonParser.console.instrumentation;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;

public class LogMap {

    private HashMap<String, LogRecord> _map = new HashMap<>();

    /* public methods */

    public String put(LogRecord record){
        String key = record.getKey();

        if(!this._map.containsKey(key))
            this._map.put(record.getKey(), record);

        return key;
    }

    public LogRecord get(String key) {
        if(!this._map.containsKey(key))
            return null;

        return this._map.get(key);
    }

    public int size() {
        return _map.size();
    }
}
