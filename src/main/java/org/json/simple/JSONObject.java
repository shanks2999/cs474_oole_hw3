/*
 * $Id: JSONObject.java,v 1.1 2006/04/15 14:10:48 platform Exp $
 * Created on 2006-4-10
 */
package org.json.simple;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * A JSON object. Key value pairs are unordered. JSONObject supports java.util.Map interface.
 * 
 * @author FangYidong<fangyidong@yahoo.com.cn>
 */
public class JSONObject extends HashMap implements Map, JSONAware, JSONStreamAware {

    private static final long serialVersionUID = -503443796854799292L;

    public JSONObject() {
        super();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONObject", "ctor", 25, "", "com.github.javaparser.ast.stmt.ExplicitConstructorInvocationStmt", "super();", "super#ctor", "public JSONObject()");
    }

    /**
	 * Allows creation of a JSONObject from a Map. After that, both the
	 * generated JSONObject and the Map can be modified independently.
	 * 
	 * @param map
	 */
    public JSONObject(Map map) {
        super(map);
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONObject", "ctor", 35, "", "com.github.javaparser.ast.stmt.ExplicitConstructorInvocationStmt", "super(map);", "super#ctor", "public JSONObject(Map map)");
    }

    /**
     * Encode a map into JSON text and write it to out.
     * If this map is also a JSONAware or JSONStreamAware, JSONAware or JSONStreamAware specific behaviours will be ignored at this top level.
     * 
     * @see org.json.simple.JSONValue#writeJSONString(Object, Writer)
     * 
     * @param map
     * @param out
     */
    public static void writeJSONString(Map map, Writer out) throws IOException {
        if (map == null) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONObject", "writeJSONString", 49, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "map == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public static void writeJSONString(Map map, Writer out) throws IOException");
            out.write("null");
            return;
        }
        boolean first = true;
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONObject", "writeJSONString", 54, "", "com.github.javaparser.ast.body.VariableDeclarator", "first = true", "", "public static void writeJSONString(Map map, Writer out) throws IOException");
        Iterator iter = map.entrySet().iterator();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONObject", "writeJSONString", 55, "", "com.github.javaparser.ast.body.VariableDeclarator", "iter = map.entrySet().iterator()", "", "public static void writeJSONString(Map map, Writer out) throws IOException");
        out.write('{');
        while (iter.hasNext()) {
            if (first) {
                first = false;
            } else {
                out.write(',');
            }
            Map.Entry entry = (Map.Entry) iter.next();
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONObject", "writeJSONString", 63, "$While", "com.github.javaparser.ast.body.VariableDeclarator", "entry = (Map.Entry) iter.next()", "", "public static void writeJSONString(Map map, Writer out) throws IOException");
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONObject", "writeJSONString", 63, "$While", "com.github.javaparser.ast.expr.CastExpr", "(Map.Entry) iter.next()", "com.github.javaparser.ast.expr.CastExpr", "public static void writeJSONString(Map map, Writer out) throws IOException");
            out.write('\"');
            out.write(escape(String.valueOf(entry.getKey())));
            out.write('\"');
            out.write(':');
            JSONValue.writeJSONString(entry.getValue(), out);
        }
        out.write('}');
    }

    public void writeJSONString(Writer out) throws IOException {
        writeJSONString(this, out);
    }

    /**
	 * Convert a map to JSON text. The result is a JSON object. 
	 * If this map is also a JSONAware, JSONAware specific behaviours will be omitted at this top level.
	 * 
	 * @see org.json.simple.JSONValue#toJSONString(Object)
	 * 
	 * @param map
	 * @return JSON text, or "null" if map is null.
	 */
    public static String toJSONString(Map map) {
        final StringWriter writer = new StringWriter();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONObject", "toJSONString", 87, "", "com.github.javaparser.ast.body.VariableDeclarator", "writer = new StringWriter()", "", "public static String toJSONString(Map map)");
        try {
            writeJSONString(map, writer);
            return writer.toString();
        } catch (IOException e) {
            // This should never happen with a StringWriter
            throw new RuntimeException(e);
        }
    }

    public String toJSONString() {
        return toJSONString(this);
    }

    public String toString() {
        return toJSONString();
    }

    public static String toString(String key, Object value) {
        StringBuffer sb = new StringBuffer();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONObject", "toString", 107, "", "com.github.javaparser.ast.body.VariableDeclarator", "sb = new StringBuffer()", "", "public static String toString(String key, Object value)");
        sb.append('\"');
        if (key == null) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONObject", "toString", 109, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "key == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public static String toString(String key, Object value)");
            sb.append("null");
        } else {
            JSONValue.escape(key, sb);
        }
        sb.append('\"').append(':');
        sb.append(JSONValue.toJSONString(value));
        return sb.toString();
    }

    /**
	 * Escape quotes, \, /, \r, \n, \b, \f, \t and other control characters (U+0000 through U+001F).
	 * It's the same as JSONValue.escape() only for compatibility here.
	 * 
	 * @see org.json.simple.JSONValue#escape(String)
	 * 
	 * @param s
	 * @return
	 */
    public static String escape(String s) {
        return JSONValue.escape(s);
    }
}
