/*
 * $Id: JSONValue.java,v 1.1 2006/04/15 14:37:04 platform Exp $
 * Created on 2006-4-15
 */
package org.json.simple;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Collection;
// import java.util.List;
import java.util.Map;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * @author FangYidong<fangyidong@yahoo.com.cn>
 */
public class JSONValue {

    /**
	 * Parse JSON text into java object from the input source. 
	 * Please use parseWithException() if you don't want to ignore the exception.
	 * 
	 * @see org.json.simple.parser.JSONParser#parse(Reader)
	 * @see #parseWithException(Reader)
	 * 
	 * @param in
	 * @return Instance of the following:
	 *	org.json.simple.JSONObject,
	 * 	org.json.simple.JSONArray,
	 * 	java.lang.String,
	 * 	java.lang.Number,
	 * 	java.lang.Boolean,
	 * 	null
	 * 
	 * @deprecated this method may throw an {@code Error} instead of returning
	 * {@code null}; please use {@link JSONValue#parseWithException(Reader)}
	 * instead
	 */
    public static Object parse(Reader in) {
        try {
            JSONParser parser = new JSONParser();
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "parse", 46, "", "com.github.javaparser.ast.body.VariableDeclarator", "parser = new JSONParser()", "", "public static Object parse(Reader in)");
            return parser.parse(in);
        } catch (Exception e) {
            return null;
        }
    }

    /**
	 * Parse JSON text into java object from the given string. 
	 * Please use parseWithException() if you don't want to ignore the exception.
	 * 
	 * @see org.json.simple.parser.JSONParser#parse(Reader)
	 * @see #parseWithException(Reader)
	 * 
	 * @param s
	 * @return Instance of the following:
	 *	org.json.simple.JSONObject,
	 * 	org.json.simple.JSONArray,
	 * 	java.lang.String,
	 * 	java.lang.Number,
	 * 	java.lang.Boolean,
	 * 	null
	 * 
	 * @deprecated this method may throw an {@code Error} instead of returning
	 * {@code null}; please use {@link JSONValue#parseWithException(String)}
	 * instead
	 */
    public static Object parse(String s) {
        java.io.StringReader in = new java.io.StringReader(s);
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "parse", 75, "", "com.github.javaparser.ast.body.VariableDeclarator", "in = new StringReader(s)", "", "public static Object parse(String s)");
        return parse(in);
    }

    /**
	 * Parse JSON text into java object from the input source.
	 * 
	 * @see org.json.simple.parser.JSONParser
	 * 
	 * @param in
	 * @return Instance of the following:
	 * 	org.json.simple.JSONObject,
	 * 	org.json.simple.JSONArray,
	 * 	java.lang.String,
	 * 	java.lang.Number,
	 * 	java.lang.Boolean,
	 * 	null
	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
    public static Object parseWithException(Reader in) throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "parseWithException", 97, "", "com.github.javaparser.ast.body.VariableDeclarator", "parser = new JSONParser()", "", "public static Object parseWithException(Reader in) throws IOException, ParseException");
        return parser.parse(in);
    }

    public static Object parseWithException(String s) throws ParseException {
        JSONParser parser = new JSONParser();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "parseWithException", 102, "", "com.github.javaparser.ast.body.VariableDeclarator", "parser = new JSONParser()", "", "public static Object parseWithException(String s) throws ParseException");
        return parser.parse(s);
    }

    /**
     * Encode an object into JSON text and write it to out.
     * <p>
     * If this object is a Map or a List, and it's also a JSONStreamAware or a JSONAware, JSONStreamAware or JSONAware will be considered firstly.
     * <p>
     * DO NOT call this method from writeJSONString(Writer) of a class that implements both JSONStreamAware and (Map or List) with 
     * "this" as the first parameter, use JSONObject.writeJSONString(Map, Writer) or JSONArray.writeJSONString(List, Writer) instead. 
     * 
     * @see org.json.simple.JSONObject#writeJSONString(Map, Writer)
     * @see org.json.simple.JSONArray#writeJSONString(List, Writer)
     * 
     * @param value
     * @param writer
     */
    public static void writeJSONString(Object value, Writer out) throws IOException {
        if (value == null) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "writeJSONString", 121, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "value == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public static void writeJSONString(Object value, Writer out) throws IOException");
            out.write("null");
            return;
        }
        if (value instanceof String) {
            out.write('\"');
            out.write(escape((String) value));
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "writeJSONString", 128, "$If", "com.github.javaparser.ast.expr.CastExpr", "(String) value", "com.github.javaparser.ast.expr.CastExpr", "public static void writeJSONString(Object value, Writer out) throws IOException");
            out.write('\"');
            return;
        }
        if (value instanceof Double) {
            if (((Double) value).isInfinite() || ((Double) value).isNaN()) {
                org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "writeJSONString", 134, "$If.$If", "com.github.javaparser.ast.expr.BinaryExpr", "((Double) value).isInfinite() || ((Double) value).isNaN()", "com.github.javaparser.ast.expr.BinaryExpr.Operator.OR", "public static void writeJSONString(Object value, Writer out) throws IOException");
                out.write("null");
            } else {
                out.write(value.toString());
            }
            return;
        }
        if (value instanceof Float) {
            if (((Float) value).isInfinite() || ((Float) value).isNaN()) {
                org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "writeJSONString", 142, "$If.$If", "com.github.javaparser.ast.expr.BinaryExpr", "((Float) value).isInfinite() || ((Float) value).isNaN()", "com.github.javaparser.ast.expr.BinaryExpr.Operator.OR", "public static void writeJSONString(Object value, Writer out) throws IOException");
                out.write("null");
            } else {
                out.write(value.toString());
            }
            return;
        }
        if (value instanceof Number) {
            out.write(value.toString());
            return;
        }
        if (value instanceof Boolean) {
            out.write(value.toString());
            return;
        }
        if ((value instanceof JSONStreamAware)) {
            ((JSONStreamAware) value).writeJSONString(out);
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "writeJSONString", 160, "$If", "com.github.javaparser.ast.expr.CastExpr", "(JSONStreamAware) value", "com.github.javaparser.ast.expr.CastExpr", "public static void writeJSONString(Object value, Writer out) throws IOException");
            return;
        }
        if ((value instanceof JSONAware)) {
            out.write(((JSONAware) value).toJSONString());
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "writeJSONString", 165, "$If", "com.github.javaparser.ast.expr.CastExpr", "(JSONAware) value", "com.github.javaparser.ast.expr.CastExpr", "public static void writeJSONString(Object value, Writer out) throws IOException");
            return;
        }
        if (value instanceof Map) {
            JSONObject.writeJSONString((Map) value, out);
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "writeJSONString", 170, "$If", "com.github.javaparser.ast.expr.CastExpr", "(Map) value", "com.github.javaparser.ast.expr.CastExpr", "public static void writeJSONString(Object value, Writer out) throws IOException");
            return;
        }
        if (value instanceof Collection) {
            JSONArray.writeJSONString((Collection) value, out);
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "writeJSONString", 175, "$If", "com.github.javaparser.ast.expr.CastExpr", "(Collection) value", "com.github.javaparser.ast.expr.CastExpr", "public static void writeJSONString(Object value, Writer out) throws IOException");
            return;
        }
        if (value instanceof byte[]) {
            JSONArray.writeJSONString((byte[]) value, out);
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "writeJSONString", 180, "$If", "com.github.javaparser.ast.expr.CastExpr", "(byte[]) value", "com.github.javaparser.ast.expr.CastExpr", "public static void writeJSONString(Object value, Writer out) throws IOException");
            return;
        }
        if (value instanceof short[]) {
            JSONArray.writeJSONString((short[]) value, out);
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "writeJSONString", 185, "$If", "com.github.javaparser.ast.expr.CastExpr", "(short[]) value", "com.github.javaparser.ast.expr.CastExpr", "public static void writeJSONString(Object value, Writer out) throws IOException");
            return;
        }
        if (value instanceof int[]) {
            JSONArray.writeJSONString((int[]) value, out);
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "writeJSONString", 190, "$If", "com.github.javaparser.ast.expr.CastExpr", "(int[]) value", "com.github.javaparser.ast.expr.CastExpr", "public static void writeJSONString(Object value, Writer out) throws IOException");
            return;
        }
        if (value instanceof long[]) {
            JSONArray.writeJSONString((long[]) value, out);
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "writeJSONString", 195, "$If", "com.github.javaparser.ast.expr.CastExpr", "(long[]) value", "com.github.javaparser.ast.expr.CastExpr", "public static void writeJSONString(Object value, Writer out) throws IOException");
            return;
        }
        if (value instanceof float[]) {
            JSONArray.writeJSONString((float[]) value, out);
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "writeJSONString", 200, "$If", "com.github.javaparser.ast.expr.CastExpr", "(float[]) value", "com.github.javaparser.ast.expr.CastExpr", "public static void writeJSONString(Object value, Writer out) throws IOException");
            return;
        }
        if (value instanceof double[]) {
            JSONArray.writeJSONString((double[]) value, out);
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "writeJSONString", 205, "$If", "com.github.javaparser.ast.expr.CastExpr", "(double[]) value", "com.github.javaparser.ast.expr.CastExpr", "public static void writeJSONString(Object value, Writer out) throws IOException");
            return;
        }
        if (value instanceof boolean[]) {
            JSONArray.writeJSONString((boolean[]) value, out);
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "writeJSONString", 210, "$If", "com.github.javaparser.ast.expr.CastExpr", "(boolean[]) value", "com.github.javaparser.ast.expr.CastExpr", "public static void writeJSONString(Object value, Writer out) throws IOException");
            return;
        }
        if (value instanceof char[]) {
            JSONArray.writeJSONString((char[]) value, out);
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "writeJSONString", 215, "$If", "com.github.javaparser.ast.expr.CastExpr", "(char[]) value", "com.github.javaparser.ast.expr.CastExpr", "public static void writeJSONString(Object value, Writer out) throws IOException");
            return;
        }
        if (value instanceof Object[]) {
            JSONArray.writeJSONString((Object[]) value, out);
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "writeJSONString", 220, "$If", "com.github.javaparser.ast.expr.CastExpr", "(Object[]) value", "com.github.javaparser.ast.expr.CastExpr", "public static void writeJSONString(Object value, Writer out) throws IOException");
            return;
        }
        out.write(value.toString());
    }

    /**
	 * Convert an object to JSON text.
	 * <p>
	 * If this object is a Map or a List, and it's also a JSONAware, JSONAware will be considered firstly.
	 * <p>
	 * DO NOT call this method from toJSONString() of a class that implements both JSONAware and Map or List with 
	 * "this" as the parameter, use JSONObject.toJSONString(Map) or JSONArray.toJSONString(List) instead. 
	 * 
	 * @see org.json.simple.JSONObject#toJSONString(Map)
	 * @see org.json.simple.JSONArray#toJSONString(List)
	 * 
	 * @param value
	 * @return JSON text, or "null" if value is null or it's an NaN or an INF number.
	 */
    public static String toJSONString(Object value) {
        final StringWriter writer = new StringWriter();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "toJSONString", 242, "", "com.github.javaparser.ast.body.VariableDeclarator", "writer = new StringWriter()", "", "public static String toJSONString(Object value)");
        try {
            writeJSONString(value, writer);
            return writer.toString();
        } catch (IOException e) {
            // This should never happen for a StringWriter
            throw new RuntimeException(e);
        }
    }

    /**
	 * Escape quotes, \, /, \r, \n, \b, \f, \t and other control characters (U+0000 through U+001F).
	 * @param s
	 * @return
	 */
    public static String escape(String s) {
        if (s == null) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "escape", 259, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "s == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public static String escape(String s)");
            return null;
        }
        StringBuffer sb = new StringBuffer();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "escape", 261, "", "com.github.javaparser.ast.body.VariableDeclarator", "sb = new StringBuffer()", "", "public static String escape(String s)");
        escape(s, sb);
        return sb.toString();
    }

    /**
     * @param s - Must not be null.
     * @param sb
     */
    static void escape(String s, StringBuffer sb) {
        final int len = s.length();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "escape", 271, "", "com.github.javaparser.ast.body.VariableDeclarator", "len = s.length()", "", "static void escape(String s, StringBuffer sb)");
        for (int i = 0; i < len; i++) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "escape", 272, "$For", "com.github.javaparser.ast.expr.UnaryExpr", "i++", "com.github.javaparser.ast.expr.UnaryExpr.Operator.POSTFIX_INCREMENT", "static void escape(String s, StringBuffer sb)");
            {
                org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "escape", 272, "$For", "com.github.javaparser.ast.expr.BinaryExpr", "i < len", "com.github.javaparser.ast.expr.BinaryExpr.Operator.LESS", "static void escape(String s, StringBuffer sb)");
                {
                    char ch = s.charAt(i);
                    org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONValue", "escape", 273, "$For", "com.github.javaparser.ast.body.VariableDeclarator", "ch = s.charAt(i)", "", "static void escape(String s, StringBuffer sb)");
                    switch(ch) {
                        case '"':
                            sb.append("\\\"");
                            break;
                        case '\\':
                            sb.append("\\\\");
                            break;
                        case '\b':
                            sb.append("\\b");
                            break;
                        case '\f':
                            sb.append("\\f");
                            break;
                        case '\n':
                            sb.append("\\n");
                            break;
                        case '\r':
                            sb.append("\\r");
                            break;
                        case '\t':
                            sb.append("\\t");
                            break;
                        case '/':
                            sb.append("\\/");
                            break;
                        default:
                            //Reference: http://www.unicode.org/versions/Unicode5.1.0/
                            if ((ch >= ' ' && ch <= '') || (ch >= '' && ch <= '') || (ch >= ' ' && ch <= '⃿')) {
                                String ss = Integer.toHexString(ch);
                                sb.append("\\u");
                                for (int k = 0; k < 4 - ss.length(); k++) {
                                    sb.append('0');
                                }
                                sb.append(ss.toUpperCase());
                            } else {
                                sb.append(ch);
                            }
                    }
                }
            }
        }
    //for
    }
}
