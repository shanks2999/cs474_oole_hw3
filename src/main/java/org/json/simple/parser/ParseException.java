package org.json.simple.parser;

/**
 * ParseException explains why and where the error occurs in source JSON text.
 * 
 * @author FangYidong<fangyidong@yahoo.com.cn>
 *
 */
public class ParseException extends Exception {

    private static final long serialVersionUID = -7880698968187728547L;

    public static final int ERROR_UNEXPECTED_CHAR = 0;

    public static final int ERROR_UNEXPECTED_TOKEN = 1;

    public static final int ERROR_UNEXPECTED_EXCEPTION = 2;

    private int errorType;

    private Object unexpectedObject;

    private int position;

    public ParseException(int errorType) {
        this(-1, errorType, null);
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.ParseException", "ctor", 21, "", "com.github.javaparser.ast.expr.UnaryExpr", "-1", "com.github.javaparser.ast.expr.UnaryExpr.Operator.MINUS", "public ParseException(int errorType)");
    }

    public ParseException(int errorType, Object unexpectedObject) {
        this(-1, errorType, unexpectedObject);
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.ParseException", "ctor", 25, "", "com.github.javaparser.ast.expr.UnaryExpr", "-1", "com.github.javaparser.ast.expr.UnaryExpr.Operator.MINUS", "public ParseException(int errorType, Object unexpectedObject)");
    }

    public ParseException(int position, int errorType, Object unexpectedObject) {
        this.position = position;
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.ParseException", "ctor", 29, "", "com.github.javaparser.ast.expr.AssignExpr", "this.position = position", "", "public ParseException(int position, int errorType, Object unexpectedObject)");
        this.errorType = errorType;
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.ParseException", "ctor", 30, "", "com.github.javaparser.ast.expr.AssignExpr", "this.errorType = errorType", "", "public ParseException(int position, int errorType, Object unexpectedObject)");
        this.unexpectedObject = unexpectedObject;
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.ParseException", "ctor", 31, "", "com.github.javaparser.ast.expr.AssignExpr", "this.unexpectedObject = unexpectedObject", "", "public ParseException(int position, int errorType, Object unexpectedObject)");
    }

    public int getErrorType() {
        return errorType;
    }

    public void setErrorType(int errorType) {
        this.errorType = errorType;
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.ParseException", "setErrorType", 39, "", "com.github.javaparser.ast.expr.AssignExpr", "this.errorType = errorType", "", "public void setErrorType(int errorType)");
    }

    /**
	 * @see org.json.simple.parser.JSONParser#getPosition()
	 * 
	 * @return The character position (starting with 0) of the input where the error occurs.
	 */
    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.ParseException", "setPosition", 52, "", "com.github.javaparser.ast.expr.AssignExpr", "this.position = position", "", "public void setPosition(int position)");
    }

    /**
	 * @see org.json.simple.parser.Yytoken
	 * 
	 * @return One of the following base on the value of errorType:
	 * 		   	ERROR_UNEXPECTED_CHAR		java.lang.Character
	 * 			ERROR_UNEXPECTED_TOKEN		org.json.simple.parser.Yytoken
	 * 			ERROR_UNEXPECTED_EXCEPTION	java.lang.Exception
	 */
    public Object getUnexpectedObject() {
        return unexpectedObject;
    }

    public void setUnexpectedObject(Object unexpectedObject) {
        this.unexpectedObject = unexpectedObject;
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.ParseException", "setUnexpectedObject", 68, "", "com.github.javaparser.ast.expr.AssignExpr", "this.unexpectedObject = unexpectedObject", "", "public void setUnexpectedObject(Object unexpectedObject)");
    }

    public String getMessage() {
        StringBuffer sb = new StringBuffer();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.ParseException", "getMessage", 72, "", "com.github.javaparser.ast.body.VariableDeclarator", "sb = new StringBuffer()", "", "public String getMessage()");
        switch(errorType) {
            case ERROR_UNEXPECTED_CHAR:
                sb.append("Unexpected character (").append(unexpectedObject).append(") at position ").append(position).append(".");
                break;
            case ERROR_UNEXPECTED_TOKEN:
                sb.append("Unexpected token ").append(unexpectedObject).append(" at position ").append(position).append(".");
                break;
            case ERROR_UNEXPECTED_EXCEPTION:
                sb.append("Unexpected exception at position ").append(position).append(": ").append(unexpectedObject);
                break;
            default:
                sb.append("Unkown error at position ").append(position).append(".");
                break;
        }
        return sb.toString();
    }
}
