/*
 * $Id: JSONParser.java,v 1.1 2006/04/15 14:10:48 platform Exp $
 * Created on 2006-4-15
 */
package org.json.simple.parser;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Parser for JSON text. Please note that JSONParser is NOT thread-safe.
 * 
 * @author FangYidong<fangyidong@yahoo.com.cn>
 */
public class JSONParser {

    public static final int S_INIT = 0;

    //string,number,boolean,null,object,array
    public static final int S_IN_FINISHED_VALUE = 1;

    public static final int S_IN_OBJECT = 2;

    public static final int S_IN_ARRAY = 3;

    public static final int S_PASSED_PAIR_KEY = 4;

    public static final int S_IN_PAIR_VALUE = 5;

    public static final int S_END = 6;

    public static final int S_IN_ERROR = -1;

    private LinkedList handlerStatusStack;

    private Yylex lexer = new Yylex((Reader) null);

    private Yytoken token = null;

    private int status = S_INIT;

    private int peekStatus(LinkedList statusStack) {
        if (statusStack.size() == 0) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "peekStatus", 39, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "statusStack.size() == 0", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "private int peekStatus(LinkedList statusStack)");
            return -1;
        }
        Integer status = (Integer) statusStack.getFirst();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "peekStatus", 41, "", "com.github.javaparser.ast.body.VariableDeclarator", "status = (Integer) statusStack.getFirst()", "", "private int peekStatus(LinkedList statusStack)");
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "peekStatus", 41, "", "com.github.javaparser.ast.expr.CastExpr", "(Integer) statusStack.getFirst()", "com.github.javaparser.ast.expr.CastExpr", "private int peekStatus(LinkedList statusStack)");
        return status.intValue();
    }

    /**
     *  Reset the parser to the initial state without resetting the underlying reader.
     *
     */
    public void reset() {
        token = null;
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "reset", 50, "", "com.github.javaparser.ast.expr.AssignExpr", "token = null", "", "public void reset()");
        status = S_INIT;
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "reset", 51, "", "com.github.javaparser.ast.expr.AssignExpr", "status = S_INIT", "", "public void reset()");
        handlerStatusStack = null;
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "reset", 52, "", "com.github.javaparser.ast.expr.AssignExpr", "handlerStatusStack = null", "", "public void reset()");
    }

    /**
     * Reset the parser to the initial state with a new character reader.
     * 
     * @param in - The new character reader.
     * @throws IOException
     * @throws ParseException
     */
    public void reset(Reader in) {
        lexer.yyreset(in);
        reset();
    }

    /**
	 * @return The position of the beginning of the current token.
	 */
    public int getPosition() {
        return lexer.getPosition();
    }

    public Object parse(String s) throws ParseException {
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "parse", 75, "", "com.github.javaparser.ast.expr.CastExpr", "(ContainerFactory) null", "com.github.javaparser.ast.expr.CastExpr", "public Object parse(String s) throws ParseException");
        return parse(s, (ContainerFactory) null);
    }

    public Object parse(String s, ContainerFactory containerFactory) throws ParseException {
        java.io.StringReader in = new java.io.StringReader(s);
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "parse", 79, "", "com.github.javaparser.ast.body.VariableDeclarator", "in = new StringReader(s)", "", "public Object parse(String s, ContainerFactory containerFactory) throws ParseException");
        try {
            return parse(in, containerFactory);
        } catch (IOException ie) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "parse", 87, "", "com.github.javaparser.ast.expr.UnaryExpr", "-1", "com.github.javaparser.ast.expr.UnaryExpr.Operator.MINUS", "public Object parse(String s, ContainerFactory containerFactory) throws ParseException");
            /*
			 * Actually it will never happen.
			 */
            throw new ParseException(-1, ParseException.ERROR_UNEXPECTED_EXCEPTION, ie);
        }
    }

    public Object parse(Reader in) throws IOException, ParseException {
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "parse", 92, "", "com.github.javaparser.ast.expr.CastExpr", "(ContainerFactory) null", "com.github.javaparser.ast.expr.CastExpr", "public Object parse(Reader in) throws IOException, ParseException");
        return parse(in, (ContainerFactory) null);
    }

    /**
	 * Parse JSON text into java object from the input source.
	 * 	
	 * @param in
     * @param containerFactory - Use this factory to createyour own JSON object and JSON array containers.
	 * @return Instance of the following:
	 *  org.json.simple.JSONObject,
	 * 	org.json.simple.JSONArray,
	 * 	java.lang.String,
	 * 	java.lang.Number,
	 * 	java.lang.Boolean,
	 * 	null
	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
    public Object parse(Reader in, ContainerFactory containerFactory) throws IOException, ParseException {
        reset(in);
        LinkedList statusStack = new LinkedList();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "parse", 113, "", "com.github.javaparser.ast.body.VariableDeclarator", "statusStack = new LinkedList()", "", "public Object parse(Reader in, ContainerFactory containerFactory) throws IOException, ParseException");
        LinkedList valueStack = new LinkedList();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "parse", 114, "", "com.github.javaparser.ast.body.VariableDeclarator", "valueStack = new LinkedList()", "", "public Object parse(Reader in, ContainerFactory containerFactory) throws IOException, ParseException");
        try {
            do {
                nextToken();
                switch(status) {
                    case S_INIT:
                        switch(token.type) {
                            case Yytoken.TYPE_VALUE:
                                status = S_IN_FINISHED_VALUE;
                                statusStack.addFirst(new Integer(status));
                                valueStack.addFirst(token.value);
                                break;
                            case Yytoken.TYPE_LEFT_BRACE:
                                status = S_IN_OBJECT;
                                statusStack.addFirst(new Integer(status));
                                valueStack.addFirst(createObjectContainer(containerFactory));
                                break;
                            case Yytoken.TYPE_LEFT_SQUARE:
                                status = S_IN_ARRAY;
                                statusStack.addFirst(new Integer(status));
                                valueStack.addFirst(createArrayContainer(containerFactory));
                                break;
                            default:
                                status = S_IN_ERROR;
                        }
                        //inner switch
                        break;
                    case S_IN_FINISHED_VALUE:
                        if (token.type == Yytoken.TYPE_EOF)
                            return valueStack.removeFirst();
                        else
                            throw new ParseException(getPosition(), ParseException.ERROR_UNEXPECTED_TOKEN, token);
                    case S_IN_OBJECT:
                        switch(token.type) {
                            case Yytoken.TYPE_COMMA:
                                break;
                            case Yytoken.TYPE_VALUE:
                                if (token.value instanceof String) {
                                    String key = (String) token.value;
                                    valueStack.addFirst(key);
                                    status = S_PASSED_PAIR_KEY;
                                    statusStack.addFirst(new Integer(status));
                                } else {
                                    status = S_IN_ERROR;
                                }
                                break;
                            case Yytoken.TYPE_RIGHT_BRACE:
                                if (valueStack.size() > 1) {
                                    statusStack.removeFirst();
                                    valueStack.removeFirst();
                                    status = peekStatus(statusStack);
                                } else {
                                    status = S_IN_FINISHED_VALUE;
                                }
                                break;
                            default:
                                status = S_IN_ERROR;
                                break;
                        }
                        //inner switch
                        break;
                    case S_PASSED_PAIR_KEY:
                        switch(token.type) {
                            case Yytoken.TYPE_COLON:
                                break;
                            case Yytoken.TYPE_VALUE:
                                statusStack.removeFirst();
                                String key = (String) valueStack.removeFirst();
                                Map parent = (Map) valueStack.getFirst();
                                parent.put(key, token.value);
                                status = peekStatus(statusStack);
                                break;
                            case Yytoken.TYPE_LEFT_SQUARE:
                                statusStack.removeFirst();
                                key = (String) valueStack.removeFirst();
                                parent = (Map) valueStack.getFirst();
                                List newArray = createArrayContainer(containerFactory);
                                parent.put(key, newArray);
                                status = S_IN_ARRAY;
                                statusStack.addFirst(new Integer(status));
                                valueStack.addFirst(newArray);
                                break;
                            case Yytoken.TYPE_LEFT_BRACE:
                                statusStack.removeFirst();
                                key = (String) valueStack.removeFirst();
                                parent = (Map) valueStack.getFirst();
                                Map newObject = createObjectContainer(containerFactory);
                                parent.put(key, newObject);
                                status = S_IN_OBJECT;
                                statusStack.addFirst(new Integer(status));
                                valueStack.addFirst(newObject);
                                break;
                            default:
                                status = S_IN_ERROR;
                        }
                        break;
                    case S_IN_ARRAY:
                        switch(token.type) {
                            case Yytoken.TYPE_COMMA:
                                break;
                            case Yytoken.TYPE_VALUE:
                                List val = (List) valueStack.getFirst();
                                val.add(token.value);
                                break;
                            case Yytoken.TYPE_RIGHT_SQUARE:
                                if (valueStack.size() > 1) {
                                    statusStack.removeFirst();
                                    valueStack.removeFirst();
                                    status = peekStatus(statusStack);
                                } else {
                                    status = S_IN_FINISHED_VALUE;
                                }
                                break;
                            case Yytoken.TYPE_LEFT_BRACE:
                                val = (List) valueStack.getFirst();
                                Map newObject = createObjectContainer(containerFactory);
                                val.add(newObject);
                                status = S_IN_OBJECT;
                                statusStack.addFirst(new Integer(status));
                                valueStack.addFirst(newObject);
                                break;
                            case Yytoken.TYPE_LEFT_SQUARE:
                                val = (List) valueStack.getFirst();
                                List newArray = createArrayContainer(containerFactory);
                                val.add(newArray);
                                status = S_IN_ARRAY;
                                statusStack.addFirst(new Integer(status));
                                valueStack.addFirst(newArray);
                                break;
                            default:
                                status = S_IN_ERROR;
                        }
                        //inner switch
                        break;
                    case S_IN_ERROR:
                        throw new ParseException(getPosition(), ParseException.ERROR_UNEXPECTED_TOKEN, token);
                }
                //switch
                if (status == S_IN_ERROR) {
                    org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "parse", 256, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "status == S_IN_ERROR", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public Object parse(Reader in, ContainerFactory containerFactory) throws IOException, ParseException");
                    throw new ParseException(getPosition(), ParseException.ERROR_UNEXPECTED_TOKEN, token);
                }
            } while (token.type != Yytoken.TYPE_EOF);
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "parse", 259, "", "com.github.javaparser.ast.expr.BinaryExpr", "token.type != Yytoken.TYPE_EOF", "com.github.javaparser.ast.expr.BinaryExpr.Operator.NOT_EQUALS", "public Object parse(Reader in, ContainerFactory containerFactory) throws IOException, ParseException");
        } catch (IOException ie) {
            throw ie;
        }
        throw new ParseException(getPosition(), ParseException.ERROR_UNEXPECTED_TOKEN, token);
    }

    private void nextToken() throws ParseException, IOException {
        token = lexer.yylex();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "nextToken", 269, "", "com.github.javaparser.ast.expr.AssignExpr", "token = lexer.yylex()", "", "private void nextToken() throws ParseException, IOException");
        if (token == null) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "nextToken", 270, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "token == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "private void nextToken() throws ParseException, IOException");
            token = new Yytoken(Yytoken.TYPE_EOF, null);
        }
    }

    private Map createObjectContainer(ContainerFactory containerFactory) {
        if (containerFactory == null) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "createObjectContainer", 275, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "containerFactory == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "private Map createObjectContainer(ContainerFactory containerFactory)");
            return new JSONObject();
        }
        Map m = containerFactory.createObjectContainer();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "createObjectContainer", 277, "", "com.github.javaparser.ast.body.VariableDeclarator", "m = containerFactory.createObjectContainer()", "", "private Map createObjectContainer(ContainerFactory containerFactory)");
        if (m == null) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "createObjectContainer", 279, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "m == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "private Map createObjectContainer(ContainerFactory containerFactory)");
            return new JSONObject();
        }
        return m;
    }

    private List createArrayContainer(ContainerFactory containerFactory) {
        if (containerFactory == null) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "createArrayContainer", 285, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "containerFactory == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "private List createArrayContainer(ContainerFactory containerFactory)");
            return new JSONArray();
        }
        List l = containerFactory.creatArrayContainer();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "createArrayContainer", 287, "", "com.github.javaparser.ast.body.VariableDeclarator", "l = containerFactory.creatArrayContainer()", "", "private List createArrayContainer(ContainerFactory containerFactory)");
        if (l == null) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "createArrayContainer", 289, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "l == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "private List createArrayContainer(ContainerFactory containerFactory)");
            return new JSONArray();
        }
        return l;
    }

    public void parse(String s, ContentHandler contentHandler) throws ParseException {
        parse(s, contentHandler, false);
    }

    public void parse(String s, ContentHandler contentHandler, boolean isResume) throws ParseException {
        StringReader in = new StringReader(s);
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "parse", 299, "", "com.github.javaparser.ast.body.VariableDeclarator", "in = new StringReader(s)", "", "public void parse(String s, ContentHandler contentHandler, boolean isResume) throws ParseException");
        try {
            parse(in, contentHandler, isResume);
        } catch (IOException ie) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "parse", 307, "", "com.github.javaparser.ast.expr.UnaryExpr", "-1", "com.github.javaparser.ast.expr.UnaryExpr.Operator.MINUS", "public void parse(String s, ContentHandler contentHandler, boolean isResume) throws ParseException");
            /*
			 * Actually it will never happen.
			 */
            throw new ParseException(-1, ParseException.ERROR_UNEXPECTED_EXCEPTION, ie);
        }
    }

    public void parse(Reader in, ContentHandler contentHandler) throws IOException, ParseException {
        parse(in, contentHandler, false);
    }

    /**
	 * Stream processing of JSON text.
	 * 
	 * @see ContentHandler
	 * 
	 * @param in
	 * @param contentHandler
	 * @param isResume - Indicates if it continues previous parsing operation.
     *                   If set to true, resume parsing the old stream, and parameter 'in' will be ignored. 
	 *                   If this method is called for the first time in this instance, isResume will be ignored.
	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
    public void parse(Reader in, ContentHandler contentHandler, boolean isResume) throws IOException, ParseException {
        if (!isResume) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "parse", 330, "$If", "com.github.javaparser.ast.expr.UnaryExpr", "!isResume", "com.github.javaparser.ast.expr.UnaryExpr.Operator.LOGICAL_COMPLEMENT", "public void parse(Reader in, ContentHandler contentHandler, boolean isResume) throws IOException, ParseException");
            reset(in);
            handlerStatusStack = new LinkedList();
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "parse", 332, "$If", "com.github.javaparser.ast.expr.AssignExpr", "handlerStatusStack = new LinkedList()", "", "public void parse(Reader in, ContentHandler contentHandler, boolean isResume) throws IOException, ParseException");
        } else {
            if (handlerStatusStack == null) {
                org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "parse", 335, "$If.$If", "com.github.javaparser.ast.expr.BinaryExpr", "handlerStatusStack == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public void parse(Reader in, ContentHandler contentHandler, boolean isResume) throws IOException, ParseException");
                isResume = false;
                org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "parse", 336, "$If.$If", "com.github.javaparser.ast.expr.AssignExpr", "isResume = false", "", "public void parse(Reader in, ContentHandler contentHandler, boolean isResume) throws IOException, ParseException");
                reset(in);
                handlerStatusStack = new LinkedList();
                org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "parse", 338, "$If.$If", "com.github.javaparser.ast.expr.AssignExpr", "handlerStatusStack = new LinkedList()", "", "public void parse(Reader in, ContentHandler contentHandler, boolean isResume) throws IOException, ParseException");
            }
        }
        LinkedList statusStack = handlerStatusStack;
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "parse", 342, "", "com.github.javaparser.ast.body.VariableDeclarator", "statusStack = handlerStatusStack", "", "public void parse(Reader in, ContentHandler contentHandler, boolean isResume) throws IOException, ParseException");
        try {
            do {
                switch(status) {
                    case S_INIT:
                        contentHandler.startJSON();
                        nextToken();
                        switch(token.type) {
                            case Yytoken.TYPE_VALUE:
                                status = S_IN_FINISHED_VALUE;
                                statusStack.addFirst(new Integer(status));
                                if (!contentHandler.primitive(token.value))
                                    return;
                                break;
                            case Yytoken.TYPE_LEFT_BRACE:
                                status = S_IN_OBJECT;
                                statusStack.addFirst(new Integer(status));
                                if (!contentHandler.startObject())
                                    return;
                                break;
                            case Yytoken.TYPE_LEFT_SQUARE:
                                status = S_IN_ARRAY;
                                statusStack.addFirst(new Integer(status));
                                if (!contentHandler.startArray())
                                    return;
                                break;
                            default:
                                status = S_IN_ERROR;
                        }
                        //inner switch
                        break;
                    case S_IN_FINISHED_VALUE:
                        nextToken();
                        if (token.type == Yytoken.TYPE_EOF) {
                            contentHandler.endJSON();
                            status = S_END;
                            return;
                        } else {
                            status = S_IN_ERROR;
                            throw new ParseException(getPosition(), ParseException.ERROR_UNEXPECTED_TOKEN, token);
                        }
                    case S_IN_OBJECT:
                        nextToken();
                        switch(token.type) {
                            case Yytoken.TYPE_COMMA:
                                break;
                            case Yytoken.TYPE_VALUE:
                                if (token.value instanceof String) {
                                    String key = (String) token.value;
                                    status = S_PASSED_PAIR_KEY;
                                    statusStack.addFirst(new Integer(status));
                                    if (!contentHandler.startObjectEntry(key))
                                        return;
                                } else {
                                    status = S_IN_ERROR;
                                }
                                break;
                            case Yytoken.TYPE_RIGHT_BRACE:
                                if (statusStack.size() > 1) {
                                    statusStack.removeFirst();
                                    status = peekStatus(statusStack);
                                } else {
                                    status = S_IN_FINISHED_VALUE;
                                }
                                if (!contentHandler.endObject())
                                    return;
                                break;
                            default:
                                status = S_IN_ERROR;
                                break;
                        }
                        //inner switch
                        break;
                    case S_PASSED_PAIR_KEY:
                        nextToken();
                        switch(token.type) {
                            case Yytoken.TYPE_COLON:
                                break;
                            case Yytoken.TYPE_VALUE:
                                statusStack.removeFirst();
                                status = peekStatus(statusStack);
                                if (!contentHandler.primitive(token.value))
                                    return;
                                if (!contentHandler.endObjectEntry())
                                    return;
                                break;
                            case Yytoken.TYPE_LEFT_SQUARE:
                                statusStack.removeFirst();
                                statusStack.addFirst(new Integer(S_IN_PAIR_VALUE));
                                status = S_IN_ARRAY;
                                statusStack.addFirst(new Integer(status));
                                if (!contentHandler.startArray())
                                    return;
                                break;
                            case Yytoken.TYPE_LEFT_BRACE:
                                statusStack.removeFirst();
                                statusStack.addFirst(new Integer(S_IN_PAIR_VALUE));
                                status = S_IN_OBJECT;
                                statusStack.addFirst(new Integer(status));
                                if (!contentHandler.startObject())
                                    return;
                                break;
                            default:
                                status = S_IN_ERROR;
                        }
                        break;
                    case S_IN_PAIR_VALUE:
                        /*
					 * S_IN_PAIR_VALUE is just a marker to indicate the end of an object entry, it doesn't proccess any token,
					 * therefore delay consuming token until next round.
					 */
                        statusStack.removeFirst();
                        status = peekStatus(statusStack);
                        if (!contentHandler.endObjectEntry())
                            return;
                        break;
                    case S_IN_ARRAY:
                        nextToken();
                        switch(token.type) {
                            case Yytoken.TYPE_COMMA:
                                break;
                            case Yytoken.TYPE_VALUE:
                                if (!contentHandler.primitive(token.value))
                                    return;
                                break;
                            case Yytoken.TYPE_RIGHT_SQUARE:
                                if (statusStack.size() > 1) {
                                    statusStack.removeFirst();
                                    status = peekStatus(statusStack);
                                } else {
                                    status = S_IN_FINISHED_VALUE;
                                }
                                if (!contentHandler.endArray())
                                    return;
                                break;
                            case Yytoken.TYPE_LEFT_BRACE:
                                status = S_IN_OBJECT;
                                statusStack.addFirst(new Integer(status));
                                if (!contentHandler.startObject())
                                    return;
                                break;
                            case Yytoken.TYPE_LEFT_SQUARE:
                                status = S_IN_ARRAY;
                                statusStack.addFirst(new Integer(status));
                                if (!contentHandler.startArray())
                                    return;
                                break;
                            default:
                                status = S_IN_ERROR;
                        }
                        //inner switch
                        break;
                    case S_END:
                        return;
                    case S_IN_ERROR:
                        throw new ParseException(getPosition(), ParseException.ERROR_UNEXPECTED_TOKEN, token);
                }
                //switch
                if (status == S_IN_ERROR) {
                    org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "parse", 508, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "status == S_IN_ERROR", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public void parse(Reader in, ContentHandler contentHandler, boolean isResume) throws IOException, ParseException");
                    throw new ParseException(getPosition(), ParseException.ERROR_UNEXPECTED_TOKEN, token);
                }
            } while (token.type != Yytoken.TYPE_EOF);
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "parse", 511, "", "com.github.javaparser.ast.expr.BinaryExpr", "token.type != Yytoken.TYPE_EOF", "com.github.javaparser.ast.expr.BinaryExpr.Operator.NOT_EQUALS", "public void parse(Reader in, ContentHandler contentHandler, boolean isResume) throws IOException, ParseException");
        } catch (IOException ie) {
            status = S_IN_ERROR;
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "parse", 514, "", "com.github.javaparser.ast.expr.AssignExpr", "status = S_IN_ERROR", "", "public void parse(Reader in, ContentHandler contentHandler, boolean isResume) throws IOException, ParseException");
            throw ie;
        } catch (ParseException pe) {
            status = S_IN_ERROR;
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "parse", 518, "", "com.github.javaparser.ast.expr.AssignExpr", "status = S_IN_ERROR", "", "public void parse(Reader in, ContentHandler contentHandler, boolean isResume) throws IOException, ParseException");
            throw pe;
        } catch (RuntimeException re) {
            status = S_IN_ERROR;
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "parse", 522, "", "com.github.javaparser.ast.expr.AssignExpr", "status = S_IN_ERROR", "", "public void parse(Reader in, ContentHandler contentHandler, boolean isResume) throws IOException, ParseException");
            throw re;
        } catch (Error e) {
            status = S_IN_ERROR;
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "parse", 526, "", "com.github.javaparser.ast.expr.AssignExpr", "status = S_IN_ERROR", "", "public void parse(Reader in, ContentHandler contentHandler, boolean isResume) throws IOException, ParseException");
            throw e;
        }
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.parser.JSONParser", "parse", 530, "", "com.github.javaparser.ast.expr.AssignExpr", "status = S_IN_ERROR", "", "public void parse(Reader in, ContentHandler contentHandler, boolean isResume) throws IOException, ParseException");
        status = S_IN_ERROR;
        throw new ParseException(getPosition(), ParseException.ERROR_UNEXPECTED_TOKEN, token);
    }
}
