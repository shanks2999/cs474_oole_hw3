/*
 * $Id: ItemList.java,v 1.1 2006/04/15 14:10:48 platform Exp $
 * Created on 2006-3-24
 */
package org.json.simple;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * |a:b:c| => |a|,|b|,|c|
 * |:| => ||,||
 * |a:| => |a|,||
 * @author FangYidong<fangyidong@yahoo.com.cn>
 */
public class ItemList {

    private String sp = ",";

    List items = new ArrayList();

    public ItemList() {
    }

    public ItemList(String s) {
        this.split(s, sp, items);
    }

    public ItemList(String s, String sp) {
        this.sp = s;
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "ctor", 30, "", "com.github.javaparser.ast.expr.AssignExpr", "this.sp = s", "", "public ItemList(String s, String sp)");
        this.split(s, sp, items);
    }

    public ItemList(String s, String sp, boolean isMultiToken) {
        split(s, sp, items, isMultiToken);
    }

    public List getItems() {
        return this.items;
    }

    public String[] getArray() {
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "getArray", 43, "", "com.github.javaparser.ast.expr.CastExpr", "(String[]) this.items.toArray()", "com.github.javaparser.ast.expr.CastExpr", "public String[] getArray()");
        return (String[]) this.items.toArray();
    }

    public void split(String s, String sp, List append, boolean isMultiToken) {
        if (s == null || sp == null) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "split", 47, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "s == null || sp == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.OR", "public void split(String s, String sp, List append, boolean isMultiToken)");
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "split", 47, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "sp == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public void split(String s, String sp, List append, boolean isMultiToken)");
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "split", 47, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "s == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public void split(String s, String sp, List append, boolean isMultiToken)");
            return;
        }
        if (isMultiToken) {
            StringTokenizer tokens = new StringTokenizer(s, sp);
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "split", 50, "$If", "com.github.javaparser.ast.body.VariableDeclarator", "tokens = new StringTokenizer(s, sp)", "", "public void split(String s, String sp, List append, boolean isMultiToken)");
            while (tokens.hasMoreTokens()) {
                append.add(tokens.nextToken().trim());
            }
        } else {
            this.split(s, sp, append);
        }
    }

    public void split(String s, String sp, List append) {
        if (s == null || sp == null) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "split", 61, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "s == null || sp == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.OR", "public void split(String s, String sp, List append)");
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "split", 61, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "sp == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public void split(String s, String sp, List append)");
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "split", 61, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "s == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public void split(String s, String sp, List append)");
            return;
        }
        int pos = 0;
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "split", 63, "", "com.github.javaparser.ast.body.VariableDeclarator", "pos = 0", "", "public void split(String s, String sp, List append)");
        int prevPos = 0;
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "split", 64, "", "com.github.javaparser.ast.body.VariableDeclarator", "prevPos = 0", "", "public void split(String s, String sp, List append)");
        do {
            prevPos = pos;
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "split", 66, "", "com.github.javaparser.ast.expr.AssignExpr", "prevPos = pos", "", "public void split(String s, String sp, List append)");
            pos = s.indexOf(sp, pos);
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "split", 67, "", "com.github.javaparser.ast.expr.AssignExpr", "pos = s.indexOf(sp, pos)", "", "public void split(String s, String sp, List append)");
            if (pos == -1) {
                org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "split", 68, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "pos == -1", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public void split(String s, String sp, List append)");
                org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "split", 68, "$If", "com.github.javaparser.ast.expr.UnaryExpr", "-1", "com.github.javaparser.ast.expr.UnaryExpr.Operator.MINUS", "public void split(String s, String sp, List append)");
                break;
            }
            append.add(s.substring(prevPos, pos).trim());
            pos += sp.length();
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "split", 71, "", "com.github.javaparser.ast.expr.AssignExpr", "pos += sp.length()", "", "public void split(String s, String sp, List append)");
        } while (pos != -1);
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "split", 72, "", "com.github.javaparser.ast.expr.BinaryExpr", "pos != -1", "com.github.javaparser.ast.expr.BinaryExpr.Operator.NOT_EQUALS", "public void split(String s, String sp, List append)");
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "split", 72, "", "com.github.javaparser.ast.expr.UnaryExpr", "-1", "com.github.javaparser.ast.expr.UnaryExpr.Operator.MINUS", "public void split(String s, String sp, List append)");
        append.add(s.substring(prevPos).trim());
    }

    public void setSP(String sp) {
        this.sp = sp;
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "setSP", 77, "", "com.github.javaparser.ast.expr.AssignExpr", "this.sp = sp", "", "public void setSP(String sp)");
    }

    public void add(int i, String item) {
        if (item == null) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "add", 81, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "item == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public void add(int i, String item)");
            return;
        }
        items.add(i, item.trim());
    }

    public void add(String item) {
        if (item == null) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "add", 87, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "item == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public void add(String item)");
            return;
        }
        items.add(item.trim());
    }

    public void addAll(ItemList list) {
        items.addAll(list.items);
    }

    public void addAll(String s) {
        this.split(s, sp, items);
    }

    public void addAll(String s, String sp) {
        this.split(s, sp, items);
    }

    public void addAll(String s, String sp, boolean isMultiToken) {
        this.split(s, sp, items, isMultiToken);
    }

    /**
	 * @param i 0-based
	 * @return
	 */
    public String get(int i) {
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "get", 113, "", "com.github.javaparser.ast.expr.CastExpr", "(String) items.get(i)", "com.github.javaparser.ast.expr.CastExpr", "public String get(int i)");
        return (String) items.get(i);
    }

    public int size() {
        return items.size();
    }

    public String toString() {
        return toString(sp);
    }

    public String toString(String sp) {
        StringBuffer sb = new StringBuffer();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "toString", 125, "", "com.github.javaparser.ast.body.VariableDeclarator", "sb = new StringBuffer()", "", "public String toString(String sp)");
        for (int i = 0; i < items.size(); i++) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "toString", 127, "$For", "com.github.javaparser.ast.expr.UnaryExpr", "i++", "com.github.javaparser.ast.expr.UnaryExpr.Operator.POSTFIX_INCREMENT", "public String toString(String sp)");
            {
                org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "toString", 127, "$For", "com.github.javaparser.ast.expr.BinaryExpr", "i < items.size()", "com.github.javaparser.ast.expr.BinaryExpr.Operator.LESS", "public String toString(String sp)");
                {
                    if (i == 0) {
                        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "toString", 128, "$For.$If", "com.github.javaparser.ast.expr.BinaryExpr", "i == 0", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public String toString(String sp)");
                        sb.append(items.get(i));
                    } else {
                        sb.append(sp);
                        sb.append(items.get(i));
                    }
                }
            }
        }
        return sb.toString();
    }

    public void clear() {
        items.clear();
    }

    public void reset() {
        sp = ",";
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.ItemList", "reset", 144, "", "com.github.javaparser.ast.expr.AssignExpr", "sp = ", "\", \"", "public void reset()");
        items.clear();
    }
}
