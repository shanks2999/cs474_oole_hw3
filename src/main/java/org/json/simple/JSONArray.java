/*
 * $Id: JSONArray.java,v 1.1 2006/04/15 14:10:48 platform Exp $
 * Created on 2006-4-10
 */
package org.json.simple;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * A JSON array. JSONObject supports java.util.List interface.
 * 
 * @author FangYidong<fangyidong@yahoo.com.cn>
 */
public class JSONArray extends ArrayList implements JSONAware, JSONStreamAware {

    private static final long serialVersionUID = 3957988303675231981L;

    /**
	 * Constructs an empty JSONArray.
	 */
    public JSONArray() {
        super();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "ctor", 26, "", "com.github.javaparser.ast.stmt.ExplicitConstructorInvocationStmt", "super();", "super#ctor", "public JSONArray()");
    }

    /**
	 * Constructs a JSONArray containing the elements of the specified
	 * collection, in the order they are returned by the collection's iterator.
	 * 
	 * @param c the collection whose elements are to be placed into this JSONArray
	 */
    public JSONArray(Collection c) {
        super(c);
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "ctor", 36, "", "com.github.javaparser.ast.stmt.ExplicitConstructorInvocationStmt", "super(c);", "super#ctor", "public JSONArray(Collection c)");
    }

    /**
     * Encode a list into JSON text and write it to out. 
     * If this list is also a JSONStreamAware or a JSONAware, JSONStreamAware and JSONAware specific behaviours will be ignored at this top level.
     * 
     * @see org.json.simple.JSONValue#writeJSONString(Object, Writer)
     * 
     * @param collection
     * @param out
     */
    public static void writeJSONString(Collection collection, Writer out) throws IOException {
        if (collection == null) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 49, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "collection == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public static void writeJSONString(Collection collection, Writer out) throws IOException");
            out.write("null");
            return;
        }
        boolean first = true;
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 54, "", "com.github.javaparser.ast.body.VariableDeclarator", "first = true", "", "public static void writeJSONString(Collection collection, Writer out) throws IOException");
        Iterator iter = collection.iterator();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 55, "", "com.github.javaparser.ast.body.VariableDeclarator", "iter = collection.iterator()", "", "public static void writeJSONString(Collection collection, Writer out) throws IOException");
        out.write('[');
        while (iter.hasNext()) {
            if (first) {
                first = false;
            } else {
                out.write(',');
            }
            Object value = iter.next();
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 64, "$While", "com.github.javaparser.ast.body.VariableDeclarator", "value = iter.next()", "", "public static void writeJSONString(Collection collection, Writer out) throws IOException");
            if (value == null) {
                org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 65, "$While.$If", "com.github.javaparser.ast.expr.BinaryExpr", "value == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public static void writeJSONString(Collection collection, Writer out) throws IOException");
                out.write("null");
                continue;
            }
            JSONValue.writeJSONString(value, out);
        }
        out.write(']');
    }

    public void writeJSONString(Writer out) throws IOException {
        writeJSONString(this, out);
    }

    /**
	 * Convert a list to JSON text. The result is a JSON array. 
	 * If this list is also a JSONAware, JSONAware specific behaviours will be omitted at this top level.
	 * 
	 * @see org.json.simple.JSONValue#toJSONString(Object)
	 * 
	 * @param collection
	 * @return JSON text, or "null" if list is null.
	 */
    public static String toJSONString(Collection collection) {
        final StringWriter writer = new StringWriter();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "toJSONString", 89, "", "com.github.javaparser.ast.body.VariableDeclarator", "writer = new StringWriter()", "", "public static String toJSONString(Collection collection)");
        try {
            writeJSONString(collection, writer);
            return writer.toString();
        } catch (IOException e) {
            // This should never happen for a StringWriter
            throw new RuntimeException(e);
        }
    }

    public static void writeJSONString(byte[] array, Writer out) throws IOException {
        if (array == null) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 101, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "array == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public static void writeJSONString(byte[] array, Writer out) throws IOException");
            out.write("null");
        } else {
            if (array.length == 0) {
                out.write("[]");
            } else {
                out.write("[");
                out.write(String.valueOf(array[0]));
                for (int i = 1; i < array.length; i++) {
                    org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 109, "$If.$If.$For", "com.github.javaparser.ast.expr.UnaryExpr", "i++", "com.github.javaparser.ast.expr.UnaryExpr.Operator.POSTFIX_INCREMENT", "public static void writeJSONString(byte[] array, Writer out) throws IOException");
                    {
                        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 109, "$If.$If.$For", "com.github.javaparser.ast.expr.BinaryExpr", "i < array.length", "com.github.javaparser.ast.expr.BinaryExpr.Operator.LESS", "public static void writeJSONString(byte[] array, Writer out) throws IOException");
                        {
                            out.write(",");
                            out.write(String.valueOf(array[i]));
                        }
                    }
                }
                out.write("]");
            }
        }
    }

    public static String toJSONString(byte[] array) {
        final StringWriter writer = new StringWriter();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "toJSONString", 119, "", "com.github.javaparser.ast.body.VariableDeclarator", "writer = new StringWriter()", "", "public static String toJSONString(byte[] array)");
        try {
            writeJSONString(array, writer);
            return writer.toString();
        } catch (IOException e) {
            // This should never happen for a StringWriter
            throw new RuntimeException(e);
        }
    }

    public static void writeJSONString(short[] array, Writer out) throws IOException {
        if (array == null) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 131, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "array == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public static void writeJSONString(short[] array, Writer out) throws IOException");
            out.write("null");
        } else {
            if (array.length == 0) {
                out.write("[]");
            } else {
                out.write("[");
                out.write(String.valueOf(array[0]));
                for (int i = 1; i < array.length; i++) {
                    org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 139, "$If.$If.$For", "com.github.javaparser.ast.expr.UnaryExpr", "i++", "com.github.javaparser.ast.expr.UnaryExpr.Operator.POSTFIX_INCREMENT", "public static void writeJSONString(short[] array, Writer out) throws IOException");
                    {
                        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 139, "$If.$If.$For", "com.github.javaparser.ast.expr.BinaryExpr", "i < array.length", "com.github.javaparser.ast.expr.BinaryExpr.Operator.LESS", "public static void writeJSONString(short[] array, Writer out) throws IOException");
                        {
                            out.write(",");
                            out.write(String.valueOf(array[i]));
                        }
                    }
                }
                out.write("]");
            }
        }
    }

    public static String toJSONString(short[] array) {
        final StringWriter writer = new StringWriter();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "toJSONString", 149, "", "com.github.javaparser.ast.body.VariableDeclarator", "writer = new StringWriter()", "", "public static String toJSONString(short[] array)");
        try {
            writeJSONString(array, writer);
            return writer.toString();
        } catch (IOException e) {
            // This should never happen for a StringWriter
            throw new RuntimeException(e);
        }
    }

    public static void writeJSONString(int[] array, Writer out) throws IOException {
        if (array == null) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 161, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "array == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public static void writeJSONString(int[] array, Writer out) throws IOException");
            out.write("null");
        } else {
            if (array.length == 0) {
                out.write("[]");
            } else {
                out.write("[");
                out.write(String.valueOf(array[0]));
                for (int i = 1; i < array.length; i++) {
                    org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 169, "$If.$If.$For", "com.github.javaparser.ast.expr.UnaryExpr", "i++", "com.github.javaparser.ast.expr.UnaryExpr.Operator.POSTFIX_INCREMENT", "public static void writeJSONString(int[] array, Writer out) throws IOException");
                    {
                        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 169, "$If.$If.$For", "com.github.javaparser.ast.expr.BinaryExpr", "i < array.length", "com.github.javaparser.ast.expr.BinaryExpr.Operator.LESS", "public static void writeJSONString(int[] array, Writer out) throws IOException");
                        {
                            out.write(",");
                            out.write(String.valueOf(array[i]));
                        }
                    }
                }
                out.write("]");
            }
        }
    }

    public static String toJSONString(int[] array) {
        final StringWriter writer = new StringWriter();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "toJSONString", 179, "", "com.github.javaparser.ast.body.VariableDeclarator", "writer = new StringWriter()", "", "public static String toJSONString(int[] array)");
        try {
            writeJSONString(array, writer);
            return writer.toString();
        } catch (IOException e) {
            // This should never happen for a StringWriter
            throw new RuntimeException(e);
        }
    }

    public static void writeJSONString(long[] array, Writer out) throws IOException {
        if (array == null) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 191, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "array == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public static void writeJSONString(long[] array, Writer out) throws IOException");
            out.write("null");
        } else {
            if (array.length == 0) {
                out.write("[]");
            } else {
                out.write("[");
                out.write(String.valueOf(array[0]));
                for (int i = 1; i < array.length; i++) {
                    org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 199, "$If.$If.$For", "com.github.javaparser.ast.expr.UnaryExpr", "i++", "com.github.javaparser.ast.expr.UnaryExpr.Operator.POSTFIX_INCREMENT", "public static void writeJSONString(long[] array, Writer out) throws IOException");
                    {
                        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 199, "$If.$If.$For", "com.github.javaparser.ast.expr.BinaryExpr", "i < array.length", "com.github.javaparser.ast.expr.BinaryExpr.Operator.LESS", "public static void writeJSONString(long[] array, Writer out) throws IOException");
                        {
                            out.write(",");
                            out.write(String.valueOf(array[i]));
                        }
                    }
                }
                out.write("]");
            }
        }
    }

    public static String toJSONString(long[] array) {
        final StringWriter writer = new StringWriter();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "toJSONString", 209, "", "com.github.javaparser.ast.body.VariableDeclarator", "writer = new StringWriter()", "", "public static String toJSONString(long[] array)");
        try {
            writeJSONString(array, writer);
            return writer.toString();
        } catch (IOException e) {
            // This should never happen for a StringWriter
            throw new RuntimeException(e);
        }
    }

    public static void writeJSONString(float[] array, Writer out) throws IOException {
        if (array == null) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 221, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "array == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public static void writeJSONString(float[] array, Writer out) throws IOException");
            out.write("null");
        } else {
            if (array.length == 0) {
                out.write("[]");
            } else {
                out.write("[");
                out.write(String.valueOf(array[0]));
                for (int i = 1; i < array.length; i++) {
                    org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 229, "$If.$If.$For", "com.github.javaparser.ast.expr.UnaryExpr", "i++", "com.github.javaparser.ast.expr.UnaryExpr.Operator.POSTFIX_INCREMENT", "public static void writeJSONString(float[] array, Writer out) throws IOException");
                    {
                        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 229, "$If.$If.$For", "com.github.javaparser.ast.expr.BinaryExpr", "i < array.length", "com.github.javaparser.ast.expr.BinaryExpr.Operator.LESS", "public static void writeJSONString(float[] array, Writer out) throws IOException");
                        {
                            out.write(",");
                            out.write(String.valueOf(array[i]));
                        }
                    }
                }
                out.write("]");
            }
        }
    }

    public static String toJSONString(float[] array) {
        final StringWriter writer = new StringWriter();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "toJSONString", 239, "", "com.github.javaparser.ast.body.VariableDeclarator", "writer = new StringWriter()", "", "public static String toJSONString(float[] array)");
        try {
            writeJSONString(array, writer);
            return writer.toString();
        } catch (IOException e) {
            // This should never happen for a StringWriter
            throw new RuntimeException(e);
        }
    }

    public static void writeJSONString(double[] array, Writer out) throws IOException {
        if (array == null) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 251, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "array == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public static void writeJSONString(double[] array, Writer out) throws IOException");
            out.write("null");
        } else {
            if (array.length == 0) {
                out.write("[]");
            } else {
                out.write("[");
                out.write(String.valueOf(array[0]));
                for (int i = 1; i < array.length; i++) {
                    org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 259, "$If.$If.$For", "com.github.javaparser.ast.expr.UnaryExpr", "i++", "com.github.javaparser.ast.expr.UnaryExpr.Operator.POSTFIX_INCREMENT", "public static void writeJSONString(double[] array, Writer out) throws IOException");
                    {
                        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 259, "$If.$If.$For", "com.github.javaparser.ast.expr.BinaryExpr", "i < array.length", "com.github.javaparser.ast.expr.BinaryExpr.Operator.LESS", "public static void writeJSONString(double[] array, Writer out) throws IOException");
                        {
                            out.write(",");
                            out.write(String.valueOf(array[i]));
                        }
                    }
                }
                out.write("]");
            }
        }
    }

    public static String toJSONString(double[] array) {
        final StringWriter writer = new StringWriter();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "toJSONString", 269, "", "com.github.javaparser.ast.body.VariableDeclarator", "writer = new StringWriter()", "", "public static String toJSONString(double[] array)");
        try {
            writeJSONString(array, writer);
            return writer.toString();
        } catch (IOException e) {
            // This should never happen for a StringWriter
            throw new RuntimeException(e);
        }
    }

    public static void writeJSONString(boolean[] array, Writer out) throws IOException {
        if (array == null) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 281, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "array == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public static void writeJSONString(boolean[] array, Writer out) throws IOException");
            out.write("null");
        } else {
            if (array.length == 0) {
                out.write("[]");
            } else {
                out.write("[");
                out.write(String.valueOf(array[0]));
                for (int i = 1; i < array.length; i++) {
                    org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 289, "$If.$If.$For", "com.github.javaparser.ast.expr.UnaryExpr", "i++", "com.github.javaparser.ast.expr.UnaryExpr.Operator.POSTFIX_INCREMENT", "public static void writeJSONString(boolean[] array, Writer out) throws IOException");
                    {
                        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 289, "$If.$If.$For", "com.github.javaparser.ast.expr.BinaryExpr", "i < array.length", "com.github.javaparser.ast.expr.BinaryExpr.Operator.LESS", "public static void writeJSONString(boolean[] array, Writer out) throws IOException");
                        {
                            out.write(",");
                            out.write(String.valueOf(array[i]));
                        }
                    }
                }
                out.write("]");
            }
        }
    }

    public static String toJSONString(boolean[] array) {
        final StringWriter writer = new StringWriter();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "toJSONString", 299, "", "com.github.javaparser.ast.body.VariableDeclarator", "writer = new StringWriter()", "", "public static String toJSONString(boolean[] array)");
        try {
            writeJSONString(array, writer);
            return writer.toString();
        } catch (IOException e) {
            // This should never happen for a StringWriter
            throw new RuntimeException(e);
        }
    }

    public static void writeJSONString(char[] array, Writer out) throws IOException {
        if (array == null) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 311, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "array == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public static void writeJSONString(char[] array, Writer out) throws IOException");
            out.write("null");
        } else {
            if (array.length == 0) {
                out.write("[]");
            } else {
                out.write("[\"");
                out.write(String.valueOf(array[0]));
                for (int i = 1; i < array.length; i++) {
                    org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 319, "$If.$If.$For", "com.github.javaparser.ast.expr.UnaryExpr", "i++", "com.github.javaparser.ast.expr.UnaryExpr.Operator.POSTFIX_INCREMENT", "public static void writeJSONString(char[] array, Writer out) throws IOException");
                    {
                        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 319, "$If.$If.$For", "com.github.javaparser.ast.expr.BinaryExpr", "i < array.length", "com.github.javaparser.ast.expr.BinaryExpr.Operator.LESS", "public static void writeJSONString(char[] array, Writer out) throws IOException");
                        {
                            out.write("\",\"");
                            out.write(String.valueOf(array[i]));
                        }
                    }
                }
                out.write("\"]");
            }
        }
    }

    public static String toJSONString(char[] array) {
        final StringWriter writer = new StringWriter();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "toJSONString", 329, "", "com.github.javaparser.ast.body.VariableDeclarator", "writer = new StringWriter()", "", "public static String toJSONString(char[] array)");
        try {
            writeJSONString(array, writer);
            return writer.toString();
        } catch (IOException e) {
            // This should never happen for a StringWriter
            throw new RuntimeException(e);
        }
    }

    public static void writeJSONString(Object[] array, Writer out) throws IOException {
        if (array == null) {
            org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 341, "$If", "com.github.javaparser.ast.expr.BinaryExpr", "array == null", "com.github.javaparser.ast.expr.BinaryExpr.Operator.EQUALS", "public static void writeJSONString(Object[] array, Writer out) throws IOException");
            out.write("null");
        } else {
            if (array.length == 0) {
                out.write("[]");
            } else {
                out.write("[");
                JSONValue.writeJSONString(array[0], out);
                for (int i = 1; i < array.length; i++) {
                    org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 349, "$If.$If.$For", "com.github.javaparser.ast.expr.UnaryExpr", "i++", "com.github.javaparser.ast.expr.UnaryExpr.Operator.POSTFIX_INCREMENT", "public static void writeJSONString(Object[] array, Writer out) throws IOException");
                    {
                        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "writeJSONString", 349, "$If.$If.$For", "com.github.javaparser.ast.expr.BinaryExpr", "i < array.length", "com.github.javaparser.ast.expr.BinaryExpr.Operator.LESS", "public static void writeJSONString(Object[] array, Writer out) throws IOException");
                        {
                            out.write(",");
                            JSONValue.writeJSONString(array[i], out);
                        }
                    }
                }
                out.write("]");
            }
        }
    }

    public static String toJSONString(Object[] array) {
        final StringWriter writer = new StringWriter();
        org.smaith2.jsonParser.console.instrumentation.TemplateClass.instrum("org.json.simple.JSONArray", "toJSONString", 359, "", "com.github.javaparser.ast.body.VariableDeclarator", "writer = new StringWriter()", "", "public static String toJSONString(Object[] array)");
        try {
            writeJSONString(array, writer);
            return writer.toString();
        } catch (IOException e) {
            // This should never happen for a StringWriter
            throw new RuntimeException(e);
        }
    }

    public String toJSONString() {
        return toJSONString(this);
    }

    /**
	 * Returns a string representation of this array. This is equivalent to
	 * calling {@link JSONArray#toJSONString()}.
	 */
    public String toString() {
        return toJSONString();
    }
}
