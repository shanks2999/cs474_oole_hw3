package org.json.simple;

import junit.framework.TestCase;
import org.junit.Before;
import org.smaith2.jsonParser.console.instrumentation.LogMap;
import org.smaith2.jsonParser.console.instrumentation.LogResult;
import org.smaith2.jsonParser.console.instrumentation.TemplateClass;

public class JSONObjectTest extends TestCase {

    @Before
    public void setUp() {
        LogResult _logResult = new LogResult(new LogMap());
        TemplateClass.init(_logResult);
    }

    public void testEmptyObjectWriteJSONString() {
        final JSONObject jsonObject = new JSONObject();

        assertEquals("{}", jsonObject.toJSONString());
    }

    public void testNullObjectWriteJSONString() {
        final JSONObject jsonObject = new JSONObject();

        assertEquals("null", jsonObject.toJSONString(null));
    }

}
